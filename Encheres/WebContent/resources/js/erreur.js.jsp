
<script>
	class Erreur {
		constructor(msg, list) {
			this.message = msg;
			this.list = list;
			this.id = list.length;
			this.makeDiv();
		};
		
		makeDiv() {
			var self = this;
			this.head_div = $("#modeleErreur").clone();
			this.head_div.removeAttr("hidden");
			this.head_div.attr("id", null);
			this.head_div.find("span[name=txt_erreur]").text(this.message);
			this.head_div.find("button[name=btn_delete]").click(function() {
				self.delete();
				self.list.splice(self.id, 1);
				if (self.list.length == 0) {
					Erreur.prototype.displayBtnClear(false);
				}
			});
			
			this.head_div.appendTo("#errorsContainer");
		}
		
		delete() {
			this.head_div.remove();
		}
		
		toString() {
			return this.id + ": " + this.message;
		}
		
		message = null;
		head_div = null;
		list = null;
		id = null;
	}
	
	Erreur.prototype.listErreur = new Array();
	
	// méthode  à appeler pour ajouter une erreur
	Erreur.prototype.add = function(msg) {
		let list = Erreur.prototype.listErreur;
		list.push(new Erreur(msg, list));
		Erreur.prototype.displayBtnClear(true);
	}
	
	// affiche/cache le bouton "Clear all"
	Erreur.prototype.displayBtnClear = function(display) {
		let list = Erreur.prototype.listErreur;
		var btn_clear = $("#btn_clear_all");
		if (display) {
			if (btn_clear.is(":hidden")) {
				btn_clear.removeAttr("hidden");
				btn_clear.click(function () { 
					
					$.each( list, function( key, erreur ) {
						console.log("on tente de delete -> " +  erreur.toString())
					    erreur.delete();
					});	 
					list = new Array();
					btn_clear.attr("hidden", true);
				});
			} 
		} else {
			btn_clear.attr("hidden", true);
		}
		//console.log(list);
	}
</script>
