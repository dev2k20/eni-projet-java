<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="fr.eni.encheres.EncheresProperties"%>
<nav class="navbar navbar-expand-md navbar-dark bg-primary">
    <a class="navbar-brand" href="./listeEncheres">ENI-Encheres</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
	    <ul class="navbar-nav mr-auto">
	    	<c:if test="${!empty sessionScope.sessionUtilisateur}">
	            <li class="nav-item active">
	                <a class="nav-link" href="./listeEncheres"><%= EncheresProperties.getProperty("encheres") %> <span class="sr-only">(current)</span></a>
	            </li>
	            <li class="nav-item">
	                <a class="nav-link" href="./venteArticle"><%= EncheresProperties.getProperty("vendreArticle") %></a>
	            </li>
	            <li class="nav-item">
	                <a class="nav-link" href="./visuProfile?pseudo=${ sessionScope.sessionUtilisateur }"><%= EncheresProperties.getProperty("profil") %> (<%= session.getAttribute("sessionUtilisateur") %>)</a>
	            </li>    
	    	</c:if>
		</ul>
        <ul class="navbar-nav">	
        	
        	<c:choose>
		    	<c:when test="${EncheresProperties.getLangue().equals('en')}">
			        <li class="nav-band">
			            <a class="nav-band" href="./changementLangue"><img width="40" height="30" loading="lazy" src="https://upload.wikimedia.org/wikipedia/en/c/c3/Flag_of_France.svg" alt="<%= EncheresProperties.getProperty("drapeauFrançais") %>"></a>
			        </li>
			    </c:when>    
			    <c:otherwise>
			       <li class="nav-band">
			            <a class="nav-band" href="./changementLangue"><img width="40" height="30" loading="lazy" src="https://upload.wikimedia.org/wikipedia/commons/a/ae/Flag_of_the_United_Kingdom.svg" alt="<%= EncheresProperties.getProperty("drapeauAnglais") %>"></a>
			        </li>
			    </c:otherwise>
			</c:choose>

            <c:choose>
		    	<c:when test="${empty sessionScope.sessionUtilisateur}">
			        <li class="nav-item">
		                <a class="nav-link" href="./connexion"><%= EncheresProperties.getProperty("connexion") %></a>
		            </li>
		            <li class="nav-item">
		                <a class="nav-link" href="./inscription"><%= EncheresProperties.getProperty("inscription") %></a>
		            </li>
			    </c:when>    
			    <c:otherwise>
			     	<li class="nav-item">
		                <a class="nav-link" href="#"><%=EncheresProperties.getProperty("credits") %> : <%= session.getAttribute("credits") %></a>
		            </li>
			       <li class="nav-item">
		                <a class="nav-link" href="./deconnexion"><%= EncheresProperties.getProperty("deconnexion") %></a>
		            </li>
			    </c:otherwise>
			</c:choose>
        </ul>
    </div>
</nav>