<style>
.footer {
  position: fixed;
  left: 0;
  bottom: 0;
  width: 100%;
  /*background-color: red;*/
  /*text-align: center;*/
  z-index:999; /* s'affiche par dessus tout */
}

.centerY {
	margin:0;
	position:absolute;
	top:50%;
	left:50%;
	-ms-transform: translate(-50%, -50%) !important;
  	transform: translate(-50%, -50%) !important;
}

#errorsContainer {
	opacity: 0.6;
}


</style>

<%@include file="../../resources/js/erreur.js.jsp" %>


<div class="footer">			  
				  
	<div id="errorsContainer" class="row">
  
  
		  	<div id="modeleErreur" class="col-md-12" hidden>
		  		<div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">  		  
				  	<div class="alert alert-danger" role="alert" style="display:inline-block;width:100%;">			  
					  <div class="row">
					  
			  			  <div class="col-md-11">
			  			  	<span name="txt_erreur">
						  		Mod�le erreur
					  		</span>
						  </div>	  
						  
						  <button name="btn_delete" type="button" class="close" aria-label="Close">
							  <span aria-hidden="true">&times;</span>
							</button>
						  			  
					  </div>		  
					</div>
		  		</div>
		  		
		  		
		  	</div>
		  	 	
		  		  	<div class="col-md-12">
		  		<div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">  		  
				  	<div class="alert alert-danger" role="alert" style="display:inline-block;width:100%;background-color:transparent !important;
				  	border-color:transparent !important;">			  
					  <div class="row">
					  
		  	  			  <div class="col-md-11">
						  		<span  id="btn_clear_all" style="font-color:grey;" hidden><button type="button" class="btn btn-danger">Clear all</button></span> 	
		
						  </div>	  
	  
					  </div>		  
					</div>
		  		</div>
		  		
		  </div>


  		
  		
	</div>
  	 	
		

</div>

<%@ page import="java.util.List"%>
<%@page import="fr.eni.encheres.messages.LecteurMessage"%>

<script>
	var Err = Erreur.prototype;
</script>

<span class="erreur">${form.erreurs['motdepasse']}</span>

			

	      	<%
				List<Integer> listeCodesErreur = (List<Integer>)request.getAttribute("listeCodesErreur");
				if(listeCodesErreur!=null)
				{
					for(int codeErreur:listeCodesErreur)
					{
			%>
						<script> 
							Err.add("<%=LecteurMessage.getMessageErreur(codeErreur)%>");
						</script>
			<%	
					}
				}
			%>

  <script>
   	//Err.add("erreur 1");
  	//Err.add("erreur 2");
  	//Err.add("erreur 3");  	
  </script>
  
  
  
  
  
  
  
  
  