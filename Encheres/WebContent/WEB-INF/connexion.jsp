<%@ page pageEncoding="UTF-8" %>
<%@ page import="java.util.List"%>
<%@page import="fr.eni.encheres.messages.LecteurMessage"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@include file="include/imports.jsp" %>
<%@page import="fr.eni.encheres.EncheresProperties"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title><%= EncheresProperties.getProperty("connexion") %></title>
        
    </head>
    <body>

		<div id="nav-placeholder">
	
		</div>
	
		<script>
			$(function(){
		     $("#nav-placeholder").load("./resources/jsp/navbar.jsp");
		   });
		</script>



<div class="container center">
    <h2 style="text-align:center"><%= EncheresProperties.getProperty("connexionInscription") %></h2>
  <form method="post" action="connexion">
    <div class="row">
    	
      <div class="vl">
        <span class="vl-innertext">or</span>
      </div>
	</div>
	<div class="row">

      <div class="col">
			<div class="hide-md-lg">
			  <p>Or sign in manually:</p>
			</div>

	        <input placeholder="Pseudo" type="text" id="pseudo" name="pseudo" value="<c:out value="${utilisateur.pseudo}"/>" size="15" maxlength="60" required />
	        <span class="erreur">${form.erreurs['motdepasse']}</span>
	        <input type="password" id="motdepasse" name="motdepasse" value="" size="20" maxlength="20" placeholder="Password" required />
	        <span class="erreur">${form.erreurs['motdepasse']}</span>
	        <input type="submit" value="<%= EncheresProperties.getProperty("connexion") %>" class="sansLabel" />
	        
	        <p class="${empty form.erreurs ? 'succes' : 'erreur'}">${form.resultat}</p>
	

      </div>
      
      
      <div class="col">
        <a href="inscription" class="fb btn">
        	<i class="fa fa-fw"></i> <%= EncheresProperties.getProperty("inscription") %>
        </a>
      </div>
      
    </div>
    
  </form>
</div>

        

	
    </body>
</html>