<%@page import="fr.eni.encheres.bll.UtilisateurManager"%>
<%@ page pageEncoding="UTF-8" %>
<%@ page import="java.util.List"%>
<%@page import="fr.eni.encheres.messages.LecteurMessage"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@include file="include/imports.jsp" %>
<%@page import="fr.eni.encheres.EncheresProperties"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Inscription</title>
    </head>
    <body>

		<div id="nav-placeholder">
	
		</div>
	
		<script>
			$(function(){
		     $("#nav-placeholder").load("./resources/jsp/navbar.jsp");
		   });
		</script>
				
		<div class="container center">
		    <div class="row">
		
		<!-- 
		      <div class="col">
					<div class="hide-md-lg">
						  <p>Or sign in manually:</p>
					</div>
			        <input placeholder="Pseudo" type="text" id="pseudo" name="pseudo" value="<c:out value="${utilisateur.pseudo}"/>" size="15" maxlength="60" />
			        <span class="erreur">${form.erreurs['pseudo']}</span>
			        
			        <input placeholder="prenom" type="text" id="prenom" name="prenom" value="<c:out value="${utilisateur.prenom}"/>" size="15" maxlength="60" />
			        <span class="erreur">${form.erreurs['prenom']}</span>
			        
			        <input placeholder="Nom" type="text" id="nom" name="nom" value="<c:out value="${utilisateur.nom}"/>" size="15" maxlength="60" />
			        <span class="erreur">${form.erreurs['nom']}</span>
			        
			        <input placeholder="email" type="text" id="email" name="email" value="<c:out value="${utilisateur.email}"/>" size="15" maxlength="60" />
			        <span class="erreur">${form.erreurs['email']}</span>
			        
			        <input placeholder="telephone" type="text" id="telephone" name="telephone" value="<c:out value="${utilisateur.telephone}"/>" size="15" maxlength="60" />
			        <span class="erreur">${form.erreurs['telephone']}</span>
			        
			        <input placeholder="rue" type="text" id="rue" name="rue" value="<c:out value="${utilisateur.rue}"/>" size="15" maxlength="60" />
			        <span class="erreur">${form.erreurs['rue']}</span>
			        
			        <input placeholder="code_postal" type="text" id="code_postal" name="code_postal" value="<c:out value="${utilisateur.code_postal}"/>" size="15" maxlength="60" />
			        <span class="erreur">${form.erreurs['code_postal']}</span>	
			        
			        <input placeholder="ville" type="text" id="ville" name="ville" value="<c:out value="${utilisateur.ville}"/>" size="15" maxlength="60" />
			        <span class="erreur">${form.erreurs['ville']}</span>	        
			        
			        <input type="password" id="motdepasse" name="motdepasse" value="" size="20" maxlength="20" placeholder="Password" />
			        <span class="erreur">${form.erreurs['motdepasse']}</span>
			        
			        <input type="submit" value="Inscription" class="sansLabel" />
			        
			        <p class="${empty form.erreurs ? 'succes' : 'erreur'}">${form.resultat}</p>
			        

		      </div>
		      
		      -->

		      
		      
   		<div class="container">
		  <h2>Inscription</h2>
		  <form action="inscription" method="post" class="needs-validation">
		    
		    <div class="row">
			    <div class="form-group col">
			      <label for="uname"><%=EncheresProperties.getProperty("pseudo")%></label>
			      <input type="text" class="form-control" id="uname" placeholder="" name="pseudo" required>
			      <div class="valid-feedback">Valid.</div>
			      <div class="invalid-feedback">Please fill out this field.</div>
			    </div>
			    <div class="form-group col">
				      <label for="uname"><%=EncheresProperties.getProperty("motDePasse")%></label>
				      <input type="password" class="form-control" id="uname" placeholder="" name="motdepasse" required>
				      <div class="valid-feedback">Valid.</div>
				      <div class="invalid-feedback">Please fill out this field.</div>
			    </div>		    
		    </div>		    

		    
		    <div class="row">
   			    <div class="form-group col">
			      <label for="uname"><%=EncheresProperties.getProperty("email")%></label>
			      <input type="text" class="form-control" id="uname" placeholder="" name="email" required>
			      <div class="valid-feedback">Valid.</div>
			      <div class="invalid-feedback">Please fill out this field.</div>
			    </div>	
		    </div>
		    
		    <div class="row">
			    <div class="form-group col">
			      <label for="uname"><%=EncheresProperties.getProperty("prenom")%></label>
			      <input type="text" class="form-control" id="uname" placeholder="" name="prenom" required>
			      <div class="valid-feedback">Valid.</div>
			      <div class="invalid-feedback">Please fill out this field.</div>
			    </div>
   			    <div class="form-group col">
				      <label for="uname"><%=EncheresProperties.getProperty("nom")%></label>
				      <input type="text" class="form-control" id="uname" placeholder="" name="nom" required>
				      <div class="valid-feedback">Valid.</div>
				      <div class="invalid-feedback">Please fill out this field.</div>
			    </div>		    
		    </div>

			<div class="row">
			    <div class="form-group col">
			      <label for="uname"><%=EncheresProperties.getProperty("telephone")%></label>
			      <input type="number" class="form-control" id="uname" placeholder="" name="telephone" required>
			      <div class="valid-feedback">Valid.</div>
			      <div class="invalid-feedback">Please fill out this field.</div>
			    </div>
			    <div class="form-group col">
			      <label for="uname"><%=EncheresProperties.getProperty("rue")%></label>
			      <input type="text" class="form-control" id="uname" placeholder="" name="rue" required>
			      <div class="valid-feedback">Valid.</div>
			      <div class="invalid-feedback">Please fill out this field.</div>
			    </div>			    
		    </div>
		    
		    <div class="row">
			    <div class="form-group col">
			      <label for="uname"><%=EncheresProperties.getProperty("ville")%></label>
			      <input type="text" class="form-control" id="uname" placeholder="" name="ville" required>
			      <div class="valid-feedback">Valid.</div>
			      <div class="invalid-feedback">Please fill out this field.</div>
			    </div>
			    <div class="form-group col">
			      <label for="uname"><%=EncheresProperties.getProperty("codePostal")%></label>
			      <input type="number" class="form-control" id="uname" placeholder="" name="code_postal" required>
			      <div class="valid-feedback">Valid.</div>
			      <div class="invalid-feedback">Please fill out this field.</div>
			    </div>			    
		    </div>
		    

		    <button type="submit" class="btn btn-primary">Submit</button>
		    
			</form>
		</div>
		
		    
		      
    </div>
		    
</div>
		
      <script>



        // Disable form submissions if there are invalid fields
        (function() {
          'use strict';
          window.addEventListener('load', function() {
            // Get the forms we want to add validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
              form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                  event.preventDefault();
                  event.stopPropagation();
                }
                form.classList.add('was-validated');
              }, false);
            });
          }, false);
        })();
        </script>

	</body>
</html>