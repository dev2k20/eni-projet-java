<%@page import="fr.eni.encheres.bo.Enchere"%>
<%@page import="fr.eni.encheres.bo.ArticleVendu"%>
<%@page import="fr.eni.encheres.bo.Categorie"%>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@include file="./include/imports.jsp" %>
<%@page import="fr.eni.encheres.EncheresProperties"%>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Map.Entry" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title><%= EncheresProperties.getProperty("accueil") %></title>
        <!-- Latest compiled and minified CSS -->
		
    </head>
    <body>	
	    <div id="nav-placeholder"></div>
	
		<script>
			$(function(){
		     $("#nav-placeholder").load("./resources/jsp/navbar.jsp");
		   });
		</script>

		<style>	    
		    input[type=radio],
		    input.radio {
		      width: auto;
		    }
		    
		    input[type=checkbox],
		    input.checkbox {
		      width: auto;
		    }
		    
		    				
				  .card-product:after {
				    content: "";
				    display: table;
				    clear: both;
				    visibility: hidden; }
				  .card-product .price-new, .card-product .price {
				    margin-right: 5px; }
				  .card-product .price-old {
				    color: #999; }
				  .card-product .img-wrap {
				    border-radius: 3px 3px 0 0;
				    overflow: hidden;
				    position: relative;
				    height: 220px;
				    text-align: center; }
				    .card-product .img-wrap img {
				      max-height: 100%;
				      max-width: 100%;
				      object-fit: cover; }
				      
				      .card-product .info-wrap {
				    overflow: hidden;
				    padding: 15px;
				    border-top: 1px solid #eee; }
				  .card-product .action-wrap {
				    padding-top: 4px;
				    margin-top: 4px; }
				  .card-product .bottom-wrap {
				    padding: 15px;
				    border-top: 1px solid #eee; }
				  .card-product .title {
				    margin-top: 0; }
		    
		    
		</style>


		<script>
			$(document).ready(function() {
				$('input:radio[name="achat_vente"]').change(
					    function(){
					        if (this.checked && this.value == 'on') {
					        	$("#encheres_ouvertes").removeAttr("disabled");
					        	$("#encheres_encours").removeAttr("disabled");
					        	$("#enchere_remportees").removeAttr("disabled");
					        	$("#ventes_encours").attr("disabled", true).prop( "checked", false );
					        	$("#ventes_non_debutees").attr("disabled", true).prop( "checked", false );
					        	$("#ventes_finies").attr("disabled", true).prop( "checked", false );
					        } else {
					        	$("#encheres_ouvertes").attr("disabled", true).prop( "checked", false );
					        	$("#encheres_encours").attr("disabled", true).prop( "checked", false );
					        	$("#enchere_remportees").attr("disabled", true).prop( "checked", false );
					        	$("#ventes_encours").removeAttr("disabled");
					        	$("#ventes_non_debutees").removeAttr("disabled");
					        	$("#ventes_finies").removeAttr("disabled");
					        }
					    }
				    );
				$("#radio_achat").attr('checked', true).trigger('change');
			})

		</script>
		
		<div class="container" style="margin-top:30px;">
		
			<h1><%=EncheresProperties.getProperty("liste_encheres")%></h1>
			
			
			<div class="row">
	
				<form method="post" action="listeEncheres" class="col-md-12 row">
					<!--  BLOC HAUT GAUCHE -->
					<div class="col-md-6">
						<div class="col-md-12">
							<span><%=EncheresProperties.getProperty("filtres")%></span>
							<input type="text" name="filtre" id="filtre"/>
						</div>
						<div class="col-md-12">
							<span><%=EncheresProperties.getProperty("categorie")%></span>
							<select name="select_categorie">
								<option value=""><%=EncheresProperties.getProperty("choose")%></option>
								<%
									List<Categorie> categories = (List<Categorie>)request.getAttribute("categories");
									for(Categorie cat:categories)
									{										
								%>
									<option value="<%=cat.getNoCategorie()%>"><%=cat.getLibelle()%></option>
								<%
									}
								%>
							</select>
						</div>
						
						<c:choose>
							<c:when test="${!empty sessionScope.sessionUtilisateur}">
							<div class="col-md-6" style="float:left;">
								  <div>
								      <input type="radio" id="radio_achat" name="achat_vente" value="on">
									  <label for="huey"><%=EncheresProperties.getProperty("achat")%></label>
								  </div>						  
								  <div>
								      <input type="checkbox" id="encheres_ouvertes" name="encheres_ouvertes">
			 						  <label for="scales"><%=EncheresProperties.getProperty("encheres_ouvertes")%></label>
		 						  </div> 						  
		 						  <div>
			 						  <input type="checkbox" id="encheres_encours" name="encheres_encours">
			 						  <label for="scales"><%=EncheresProperties.getProperty("mes_encheres_encours")%></label>
		 						  </div> 						  
		 						  <div>
			 						  <input type="checkbox" id="enchere_remportees" name="enchere_remportees">
			 						  <label for="scales"><%=EncheresProperties.getProperty("mes_encheres_win")%></label>
		 						  </div>
							</div>
							<div class="col-md-6" style="float:left;">
								  <div>
								      <input type="radio" id="radio_vente" name="achat_vente" value="off">
									  <label for="huey"><%=EncheresProperties.getProperty("mes_ventes")%></label>
								  </div>						  
								  <div>
								      <input type="checkbox" id="ventes_encours" name="ventes_encours">
			 						  <label for="scales"><%=EncheresProperties.getProperty("mes_ventes_encours")%></label>
		 						  </div> 						  
		 						  <div>
			 						  <input type="checkbox" id="ventes_non_debutees" name="ventes_non_debutees">
			 						  <label for="scales"><%=EncheresProperties.getProperty("ventes_nn_debut")%></label>
		 						  </div> 						  
		 						  <div>
			 						  <input type="checkbox" id="ventes_finies" name="ventes_finies">
			 						  <label for="scales"><%=EncheresProperties.getProperty("ventes_terminees")%></label>
		 						  </div>
							</div>	
							</c:when>    
                			<c:otherwise>
                			</c:otherwise> 
						</c:choose>
					</div>
					
					
					<!--  BLOC HAUT DROITE -->
					<div class="col-md-6">
						<div class="center">
							<input type="submit" value="Rechercher">
						</div>
					</div>
				</form>
				
				
				<hr>
				
				<!--  BLOC RESULTAT  -->
				<div class="container" style="margin:10px;">
					<div class="row">
					
							<%
							HashMap<ArticleVendu, Enchere> list = (HashMap<ArticleVendu, Enchere>)request.getAttribute("arrayEncheres");
								if(list!=null && list.size() > 0)
								{
									for(Entry<ArticleVendu, Enchere> h : list.entrySet()) {
							%>
										<div class="col-md-3">
											<figure class="card card-product">
												<div class="img-wrap"><img src="https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg"> 
												</div>
												<figcaption class="info-wrap">
													<h5 class="title text-dots"><a href="#"> <%= h.getKey().getNomArticle() %> </a></h5>
													<h6 class="title text-dots"><span><%=EncheresProperties.getProperty("fin")%> <%= h.getKey().getDateFinEncheres().toString() %> </span></h6>
													<h6 class="title text-dots"><span><%=EncheresProperties.getProperty("vendeur")%> <a href="./visuProfile?pseudo=<%= h.getKey().getVendeur().getPseudo() %>"> <%= h.getKey().getVendeur().getPseudo() %> </a></span></h6>
													<h6 class="title text-dots"><span><%=EncheresProperties.getProperty("categorie")%> <%= h.getKey().getCategorie().getLibelle() %></span></h6>
													
													
													<% if (h.getValue() != null) { %>
														<h6 class="title text-dots"><span><%=EncheresProperties.getProperty("dernier_encherisseur")%> <a href="./visuProfile?pseudo=<%= h.getValue().getEncherisseur().getPseudo() %>"><%= h.getValue().getEncherisseur().getPseudo() %></a></span></h6>
														
														<div class="action-wrap">
															<form action="detailArticle" method="get" >
																<input type="hidden" name="noArticle" value="<%= h.getKey().getNoArticle() %>" />
																<button type="submit" class="btn btn-primary btn-sm float-right"> <%=EncheresProperties.getProperty("encherir")%> </button>
															</form>	
															<div class="price-wrap h5">
																<span class="price-new"><%= h.getValue().getMontantEnchere() %> <%=EncheresProperties.getProperty("credits")%></span>
															</div> <!-- price-wrap.// -->
														</div> <!-- action-wrap -->
													<% } else { %>
														<h6 class="title text-dots"><span> <%=EncheresProperties.getProperty("aucune_enchere")%> </span></h6>
														
														<div class="action-wrap">
															<form action="detailArticle" method="get" >
																<input type="hidden" name="noArticle" value="<%= h.getKey().getNoArticle() %>" />
																<button type="submit" class="btn btn-primary btn-sm float-right"> <%=EncheresProperties.getProperty("encherir")%> </button>
															</form>	
															<div class="price-wrap h5">
																<span class="price-new"><%= h.getKey().getPrixVente() %> <%=EncheresProperties.getProperty("credits")%></span>
															</div> <!-- price-wrap.// -->
														</div> <!-- action-wrap -->
													<% } %>
													
													
												</figcaption>
											</figure> <!-- card // -->
										</div> <!-- col // -->
										
							<%	
									}
								} else {
							%>
								Aucun résultat
							<%
								}
							%>
						

					</div>
					
				</div>
				
				

				
				
			</div>
			
		</div>
		
		
		
		
		
		
		
		
		
    </body>
</html>