<%@page import="java.util.Map"%>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@include file="include/imports.jsp" %>
<%@page import="fr.eni.encheres.EncheresProperties"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title><%= EncheresProperties.getProperty("vendreArticle") %></title>
        <!-- Latest compiled and minified CSS -->
		
    </head>
    <body>	
		<%@include file="../resources/jsp/navbar.jsp" %>
			
		<br>
		

		<div class="container">
		  <h2><%= EncheresProperties.getProperty("vendreArticle") %></h2>
		  <form action="venteArticle" enctype="multipart/form-data" class="needs-validation" method="post" novalidate>
		    <div class="row">
			    <div class="form-group col-md-6">
			      <label for="nom_article"><%= EncheresProperties.getProperty("nomArticle") %></label>
			      <input type="text" class="form-control" id="nom_article" name="nom_article" required>
			      
			      <div class="invalid-feedback"><%= EncheresProperties.getProperty("renseignerChamp") %></div>
			    </div>
			    <div class="form-group col-md-6">
			      <label for="no_categorie"><%= EncheresProperties.getProperty("categorie") %></label>
			      <!--<input type="text" class="form-control" id="no_categorie" name="no_categorie" required>-->
			      
			      <select class="form-control"  id="no_categorie" name="no_categorie" required>
			      
			      	<%
			      	Map<Integer, String> categories = (Map<Integer, String>)request.getAttribute("listeCategories");
			    	if(categories != null) 
			    	{
			    		for (Map.Entry<Integer, String> entry : categories.entrySet())
			    		{
					    	%>
					    	<option value="<%= entry.getKey() %>"><%= entry.getValue() %></option>		
							<%
			    		}
			    	};
			      %>
			      </select>
			      
			  
					
			      <div class="invalid-feedback"><%= EncheresProperties.getProperty("renseignerChamp") %></div>
			    </div>
			    
		    </div>
		    <div class="row">
			    <div class="form-group col-md-12">
				      <label for="description"><%= EncheresProperties.getProperty("description") %></label>
				      <textarea class="form-control" rows="3" id="description" name="description" required></textarea>
				      <div class="invalid-feedback"><%= EncheresProperties.getProperty("renseignerChamp") %></div>
				</div>
			</div>
			
			<div class="row">
			    <div class="form-group col-md-12">
				      <label for="image"><%= EncheresProperties.getProperty("image") %></label>
				      <input type="file" name="image" id="image"/>
				</div>
			</div>
		    
		    <div class="row">
		    	<div class="form-group col-md-4">
			      <label for="prix_initial"><%= EncheresProperties.getProperty("prixDepart") %></label>
			      <input type="number" class="form-control" id="prix_initial" name="prix_initial" required>
			      
			      <div class="invalid-feedback"><%= EncheresProperties.getProperty("renseignerChamp") %></div>
			    </div>
			    <div class="form-group col-md-4">
			    	<label for="datetimepicker7"><%= EncheresProperties.getProperty("dateOuverture") %></label>
			      	<div class="input-group date" id="datetimepicker7" data-target-input="nearest">
		                <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker7" name="date_debut_encheres" required/>
		                <div class="input-group-append" data-target="#datetimepicker7" data-toggle="datetimepicker">
		                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
		                </div>
		            </div>
			      <div class="invalid-feedback"><%= EncheresProperties.getProperty("renseignerChamp") %></div>
			    </div>
			    <div class="form-group col-md-4">
			    	<label for="datetimepicker8"><%= EncheresProperties.getProperty("dateFermeture") %></label>
			      	<div class="input-group date" id="datetimepicker8" data-target-input="nearest">
		                <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker8" name="date_fin_encheres" required/>
		                <div class="input-group-append" data-target="#datetimepicker8" data-toggle="datetimepicker">
		                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
		                </div>
		            </div>
			      
			      <div class="invalid-feedback"><%= EncheresProperties.getProperty("renseignerChamp") %></div>
			    </div>
		    </div>
		    
		    <div class="row">
		    	<b><%= EncheresProperties.getProperty("adresseRetrait") %></b>
		    </div>
		    <div class="row">
			    <div class="form-group col-md-4">
			      <label for="rue"><%= EncheresProperties.getProperty("rue") %></label>
			      
			      <% String rue = (String) request.getAttribute("rue"); %>
				  <input type="text" class="form-control" id="rue" name="rue" value="<%= rue %>" required>			      		

			      
			      <div class="invalid-feedback"><%= EncheresProperties.getProperty("renseignerChamp") %></div>
			    </div>
			    <div class="form-group col-md-4">
			      <label for="ville"><%= EncheresProperties.getProperty("ville") %></label>
			      <% String ville = (String) request.getAttribute("ville"); %>
			      <input type="text" class="form-control" id="ville" name="ville" value="<%= ville %>" required>
			      
			      <div class="invalid-feedback"><%= EncheresProperties.getProperty("renseignerChamp") %></div>
			    </div>
			    <div class="form-group col-md-4">
			      <label for="code_postal"><%= EncheresProperties.getProperty("codePostal") %></label>
			      <% String cp = (String) request.getAttribute("code_postal"); %>
			      <input type="text" class="form-control" id="code_postal" name="code_postal" value="<%= cp %>" required>
			      
			      <div class="invalid-feedback"><%= EncheresProperties.getProperty("renseignerChamp") %></div>
			    </div>
		    </div>
		    
		    <button type="submit" value="venteArticle" class="btn btn-primary"><%= EncheresProperties.getProperty("creerVente") %></button>
		    
		  </form>
		</div>
		
		
		<script type="text/javascript">
			
	    $(function () {
	        $('#datetimepicker7').datetimepicker({
	        	locale: 'fr'
	        });
	        $('#datetimepicker8').datetimepicker({
	        	locale: 'fr',
	        	useCurrent: false
	        });
	        $("#datetimepicker7").on("change.datetimepicker", function (e) {
	            $('#datetimepicker8').datetimepicker('minDate', e.date);
	        });
	        $("#datetimepicker8").on("change.datetimepicker", function (e) {
	            $('#datetimepicker7').datetimepicker('maxDate', e.date);
	        });
	    });
		
		</script>
	
		
		<script>
		
		
		
		// Disable form submissions if there are invalid fields
		(function() {
		  'use strict';
		  window.addEventListener('load', function() {
		    // Get the forms we want to add validation styles to
		    var forms = document.getElementsByClassName('needs-validation');
		    // Loop over them and prevent submission
		    var validation = Array.prototype.filter.call(forms, function(form) {
		      form.addEventListener('submit', function(event) {
		        if (form.checkValidity() === false) {
		          event.preventDefault();
		          event.stopPropagation();
		        }
		        form.classList.add('was-validated');
		      }, false);
		    });
		  }, false);
		})();
		</script>		
		
    </body>
</html>