<%@ page pageEncoding="UTF-8" %>
<%@ page import="java.util.List"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Map"%>
<%@page import="fr.eni.encheres.messages.LecteurMessage"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@include file="include/imports.jsp" %>
<%@page import="fr.eni.encheres.EncheresProperties"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title><%= EncheresProperties.getProperty("") %></title>
        
    </head>
    <body>

		<div id="nav-placeholder">
	
		</div>
	
		<script>
			$(function(){
		     $("#nav-placeholder").load("./resources/jsp/navbar.jsp");
		   });
		</script>
		
		
   		<%
			HashMap<String, String> infos = (HashMap<String, String>) request.getAttribute("userInfos");
			
		%>
				
		<div class="container center">
		    <div class="">

				<%
					if(infos!=null)
					{
				%>
				<div class="col-md-12">
					<h1 style="text-align:center"><span class="badge badge-info">Profil de <%= infos.get("pseudo") %></span></h1>
				</div>
	      		
				
					  <hr style=" border-top: 1px solid #ccc;">
        	      	
				      <div class="row">
				      
				      		<%
				      			List<String> l = new ArrayList<String>(infos.keySet());
				      		
					      		for(Map.Entry<String, String> entry : infos.entrySet()) {
					      			String key = entry.getKey();
					      		    String value = entry.getValue();
					      			if (!key.equals("pseudo")) {					      		    
		
					      	%>
					      		<div class="col-md-6">
	   							  <div class="form-group row" style="display: block;vertical-align: middle;">
								    <label for="staticEmail" class="col-sm-2 col-form-label"  style="vertical-align:middle;">
								    	<%= key %>
								    </label>
								    <div class="col-sm-10">
								      <input type="text" readonly class="form-control" value="<%= value %>">
								    </div>
								  </div>
							  </div>
					      	<%
					      			}
					      		    
					      		}
				      		
				      		%>
	
							<div class="row col-md-12">
							<c:if test="${!empty sessionScope.sessionUtilisateur}">							
								<c:set var="anotherTest" value="${sessionScope.sessionUtilisateur}"/>
								<% if (pageContext.getAttribute("anotherTest").equals(infos.get("pseudo"))) { %>
					      		<div class="col-md-6">	   			
					      			<a href="./editProfile">
					      				<button type="button" class="btn btn-primary">Modifier mon profil</button>
					      			</a>		
							  	</div>
								<% } %>
							</c:if>
							</div>
							
				      </div>
			      
   				<%
					} else {
				%>
				
				<p>Aucun utilisateur trouvé.</p>
				
				<%
					}
				%>
			
		      
		      
		    </div>

		</div>
		


	</body>
</html>