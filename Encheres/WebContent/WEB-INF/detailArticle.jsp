<%@page import="java.util.Map"%>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@include file="include/imports.jsp" %>
<%@page import="fr.eni.encheres.EncheresProperties"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title><%= EncheresProperties.getProperty("detailVente") %></title>
    </head>
    <body>	
		<%@include file="../resources/jsp/navbar.jsp" %>
		<% 	String nom = (String) request.getAttribute("nom_article"); 
			String categorie = (String) request.getAttribute("categorie");
			String description = (String) request.getAttribute("description");
			int prix_initial = Integer.valueOf((String) request.getAttribute("prix_initial"));
			String no_article = (String) request.getAttribute("no_article");
			String date_fin_encheres = (String) request.getAttribute("date_fin_encheres");
			String date_debut_encheres = (String) request.getAttribute("date_debut_encheres");
			String rue = (String) request.getAttribute("rue");
			String ville = (String) request.getAttribute("ville");
			String code_postal = (String) request.getAttribute("code_postal");
			String pseudo = (String) request.getAttribute("pseudo");
			String chemin = (String) request.getAttribute("chemin_image");
			
			int prix_enchere = 0;
			int prix_proposition = 0;
			try{
				prix_enchere = Integer.valueOf((String) request.getAttribute("prix_enchere"));
				
				if(prix_enchere == 0)
					prix_proposition = prix_initial + 10;
				else
					prix_proposition = prix_enchere + 10;	
			} catch (Exception e){
				prix_proposition = prix_initial + 10;
			}

		%>
		<div class="container">
		  <h2><%= EncheresProperties.getProperty("detailVente") %></h2>
		  <form action="detailArticle" method="post" novalidate>
		   	 <div class="row">
			    <div class="form-group col-md-6">
			      <label for="nom_article"><%= EncheresProperties.getProperty("nomArticle") %></label>
			      <input type="text" class="form-control" id="nom_article" name="nom_article" value="<%= nom %>" readonly required>
			      <div class="invalid-feedback"><%= EncheresProperties.getProperty("renseignerChamp") %></div>
			    </div>
			    <div class="form-group col-md-6">
			      <label for="no_categorie"><%= EncheresProperties.getProperty("categorie") %></label>
			      <!--<input type="text" class="form-control" id="no_categorie" name="no_categorie" required>-->
			      <input class="form-control"  id="no_categorie" name="no_categorie" value="<%= categorie %>" readonly required>
			      <div class="invalid-feedback"><%= EncheresProperties.getProperty("renseignerChamp") %></div>
			    </div>
			    
		    </div>
		    <div class="row">
			    <div class="form-group col-md-12">
				      <label for="description"><%= EncheresProperties.getProperty("description") %></label>
				      <textarea class="form-control" rows="3" id="description" name="description" readonly required><%= description %></textarea>
				      <div class="invalid-feedback"><%= EncheresProperties.getProperty("renseignerChamp") %></div>
				</div>
			</div>
		    
		    <div class="row">
		    	<div class="form-group col-md-4">
			      <label for="prix_initial"><%= EncheresProperties.getProperty("prixDepart") %></label>
			      <input type="number" class="form-control" id="prix_initial" name="prix_initial" value="<%= prix_initial %>" readonly required>
			      <div class="invalid-feedback"><%= EncheresProperties.getProperty("renseignerChamp") %></div>
			    </div>
			    <div class="form-group col-md-4">
				    <c:if test="${prix_enchere != 0}">
			            <label for="prix_enchere"><%= EncheresProperties.getProperty("prix_enchere") %></label>
				      	<input type="number" class="form-control" id="prix_enchere" name="prix_enchere" value="<%= prix_enchere %>" readonly required>
				     	<div class="invalid-feedback"><%= EncheresProperties.getProperty("renseignerChamp") %></div>   
			    	</c:if>
		    	</div>
		    </div>
		     <div class="row">
			    <div class="form-group col-md-4">
			      <label for="dateOuverture"><%= EncheresProperties.getProperty("dateOuverture") %></label>
			      <input type="text" class="form-control" id="dateOuverture" name="dateOuverture" value="<%= date_debut_encheres %>" readonly required>
			      <div class="invalid-feedback"><%= EncheresProperties.getProperty("renseignerChamp") %></div>
			    </div>
			    <div class="form-group col-md-4">
			      <label for="dateFermeture"><%= EncheresProperties.getProperty("dateFermeture") %></label>
			      <input type="text" class="form-control" id="dateFermeture" name="dateFermeture" value="<%= date_fin_encheres %>" readonly required>
			      <div class="invalid-feedback"><%= EncheresProperties.getProperty("renseignerChamp") %></div>
			    </div>
		    </div>
		    
		    <div class="row">
		    	<b><%= EncheresProperties.getProperty("adresseRetrait") %></b>
		    </div>
		    <div class="row">
			    <div class="form-group col-md-4">
			      <label for="rue"><%= EncheresProperties.getProperty("rue") %></label>
				  <input type="text" class="form-control" id="rue" name="rue" value="<%= rue %>" readonly required>			      		

			      
			      <div class="invalid-feedback"><%= EncheresProperties.getProperty("renseignerChamp") %></div>
			    </div>
			    <div class="form-group col-md-4">
			      <label for="ville"><%= EncheresProperties.getProperty("ville") %></label>
			      <input type="text" class="form-control" id="ville" name="ville" value="<%= ville %>" readonly required>
			      
			      <div class="invalid-feedback"><%= EncheresProperties.getProperty("renseignerChamp") %></div>
			    </div>
			    <div class="form-group col-md-4">
			      <label for="code_postal"><%= EncheresProperties.getProperty("codePostal") %></label>
			      <input type="text" class="form-control" id="code_postal" name="code_postal" value="<%= code_postal %>" readonly required>
			      
			      <div class="invalid-feedback"><%= EncheresProperties.getProperty("renseignerChamp") %></div>
			    </div>
		    </div>
		    <input type="hidden" value="<%= no_article %>" name="no_article"/>
		     <div class="row">
		    	<div class="form-group col-md-2">
			      <%= EncheresProperties.getProperty("proposition") %>
			    </div>
			    <div class="form-group col-md-2">
			      <input type="number" class="form-control" id="prix_proposition" name="prix_proposition" value="<%= prix_proposition %>" required>
			      <div class="invalid-feedback"><%= EncheresProperties.getProperty("renseignerChamp") %></div>
			    </div>
			    <div class="form-group col-md-2">
					<button type="submit" class="btn btn-primary"><%= EncheresProperties.getProperty("encherir") %></button>			      
			    </div>
		    </div>
		    
		    
		    
		  </form>
		</div>
	
		
		<script>
		
		
		
		// Disable form submissions if there are invalid fields
		(function() {
		  'use strict';
		  window.addEventListener('load', function() {
		    // Get the forms we want to add validation styles to
		    var forms = document.getElementsByClassName('needs-validation');
		    // Loop over them and prevent submission
		    var validation = Array.prototype.filter.call(forms, function(form) {
		      form.addEventListener('submit', function(event) {
		        if (form.checkValidity() === false) {
		          event.preventDefault();
		          event.stopPropagation();
		        }
		        form.classList.add('was-validated');
		      }, false);
		    });
		  }, false);
		})();
		</script>		
    </body>
</html>