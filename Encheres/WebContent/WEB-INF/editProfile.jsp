<%@ page pageEncoding="UTF-8" %>
<%@ page import="java.util.List"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Map"%>
<%@page import="fr.eni.encheres.messages.LecteurMessage"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@include file="include/imports.jsp" %>
<%@page import="fr.eni.encheres.EncheresProperties"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title><%= EncheresProperties.getProperty("connexion") %></title>
        
    </head>
    <body>

		<div id="nav-placeholder">
	
		</div>
	
		<script>
			$(function(){
		     $("#nav-placeholder").load("./resources/jsp/navbar.jsp");
		   });
		</script>


   		<%
			HashMap<String, String> infos = (HashMap<String, String>) request.getAttribute("userInfos");
			
		%>
				
		<div class="container center">
		  <form method="post" action="editProfile">
		    <div class="">

				<%
					if(infos!=null)
					{
				%>
				<div class="col-md-12" style="margin-top:10px !important;">
					<h1 style="text-align:center"><span class="badge badge-primary">Profil de <%= infos.get("pseudo") %></span></h1>
				</div>
	      		
				
					<hr style=" border-top: 1px solid #ccc;">
        	      	
			      <div class="row">
				      		<%				      		
				      		for(Map.Entry<String, String> entry : infos.entrySet()) {
				      			String key = entry.getKey();
				      		    String value = entry.getValue();
				      			if (!key.equals("pseudo")) {
		
					      	%>
					      		<div class="col-md-6">
	   							  <div class="form-group row" style="display: block;vertical-align: middle;">
								    <label for="staticEmail" class="col-sm-2 col-form-label"  style="vertical-align:middle;">
								    	<%= key %>
								    </label>
								    <div class="col-sm-10">
								      <input type="text" class="form-control" id="<%=key%>" name="<%=key%>" value="<%= value %>">
								    </div>
								  </div>
							  </div>
					      	<%
				      			}					      		    
				      		}
				      		
				      		%>
				      		
				      		<div class="row col-md-12">
					      		<div class="col-md-6">	   							  
									<button type="submit" class="btn btn-primary">Modifier mon profil</button>
							  	</div>
							  	
									
									
								</div>
							</div>



   				<%
					} else {
				%>
				
				<p>Aucun utilisateur trouvé.</p>
				
				<%
					}
				%>
			
		      
		      
		    </div>
		   
		  </form>
		  

		</div>
		
	   <!-- Modal -->
		<div class="modal fade" id="suppressionModal" tabindex="-1" role="dialog" aria-labelledby="suppressionModalLabel" aria-hidden="true">
		    <div class="modal-dialog" role="document">
		        <div class="modal-content">
		              <div class="modal-header">
		                <h5 class="modal-title" id="suppressionModalLabel">Suppression du compte</h5>
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		                  <span aria-hidden="true">&times;</span>
		                </button>
		              </div>
		              <div class="modal-body">
		                Êtes-vous vraiment sûr de vouloir supprimer votre compte ?
		              </div>
		              <div class="modal-footer">
		                <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
		                <a href="./suppressionUtilisateur" ><button type="button" class="btn btn-danger">Supprimer</button></a>
		              </div>
		        </div>
		    </div>
		</div>
							

	</body>
</html>