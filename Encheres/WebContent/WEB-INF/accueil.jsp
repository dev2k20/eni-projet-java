<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@include file="include/imports.jsp" %>
<%@page import="fr.eni.encheres.EncheresProperties"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title><%= EncheresProperties.getProperty("accueil") %></title>
        <!-- Latest compiled and minified CSS -->
		
    </head>
    <body>	
		<%@include file="../resources/jsp/navbar.jsp" %>
			
		<br>
		<!-- Modal -->
		<div class="modal fade" id="suppressionModal" tabindex="-1" role="dialog" aria-labelledby="suppressionModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="suppressionModalLabel"><%= EncheresProperties.getProperty("suppressionCompte") %></h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		        <%= EncheresProperties.getProperty("confirmationSuppressionCompte") %>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal"><%= EncheresProperties.getProperty("annuler") %></button>
		        <a href="./suppressionUtilisateur" ><button type="button" class="btn btn-danger"><%= EncheresProperties.getProperty("supprimer") %></button></a>
		      </div>
		    </div>
		  </div>
		</div>

		<script>
		
		
		
		// Disable form submissions if there are invalid fields
		(function() {
		  'use strict';
		  window.addEventListener('load', function() {
		    // Get the forms we want to add validation styles to
		    var forms = document.getElementsByClassName('needs-validation');
		    // Loop over them and prevent submission
		    var validation = Array.prototype.filter.call(forms, function(form) {
		      form.addEventListener('submit', function(event) {
		        if (form.checkValidity() === false) {
		          event.preventDefault();
		          event.stopPropagation();
		        }
		        form.classList.add('was-validated');
		      }, false);
		    });
		  }, false);
		})();
		</script>		
		
    </body>
</html>