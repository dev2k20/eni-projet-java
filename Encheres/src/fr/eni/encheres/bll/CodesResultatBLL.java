package fr.eni.encheres.bll;

/**
 * Les codes disponibles sont entre 20000 et 29999
 */
public abstract class CodesResultatBLL {
	
	/**
	 * Echec quand la description de l'avis ne repsecte pas les règles définies
	 */
	public static final int REGLE_UTILISATEUR_PSEUDO_ERREUR=20000;
	/**
	 * Echec quand la description de l'avis ne repsecte pas les règles définies
	 */
	public static final int REGLE_UTILISATEUR_MOTDEPASSE_ERREUR=20001;

	public static final int REGLE_UTILISATEUR_PRENOM_ERREUR=20002;
	public static final int REGLE_UTILISATEUR_NOM_ERREUR=20003;
	public static final int REGLE_UTILISATEUR_EMAIL_ERREUR=20004;
	public static final int REGLE_UTILISATEUR_TELEPHONE_ERREUR=20005;
	public static final int REGLE_UTILISATEUR_RUE_ERREUR=20006;
	public static final int REGLE_UTILISATEUR_CODEPOSTAL_ERREUR=20007;
	public static final int REGLE_UTILISATEUR_VILLE_ERREUR=20008;
	
	public static final int REGLE_UTILISATEUR_PSEUDO_DEJA_EXISTANT = 20009;
	public static final int REGLE_UTILISATEUR_TELEPHONE_NUMERIC = 20010;
	
	public static final int REGLE_CONVERSION = 20011;


}
