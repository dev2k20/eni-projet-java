package fr.eni.encheres.bll;

import javax.servlet.http.HttpServletRequest;

public abstract class Manager {
	
	protected static final String ATT_USER = "utilisateur";
	
	protected static String getValeurChamp( HttpServletRequest request, String nomChamp ) {
        String valeur = request.getParameter( nomChamp );
        if ( valeur == null || valeur.trim().length() == 0 ) {
            return null;
        } else if (valeur.equals("on")) { // checkbox
        	return "true";
        } else {
            return valeur;
        }
    }
}
