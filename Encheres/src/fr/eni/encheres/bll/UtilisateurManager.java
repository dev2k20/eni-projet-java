package fr.eni.encheres.bll;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.Session;

import fr.eni.encheres.BusinessException;
import fr.eni.encheres.bo.Utilisateur;
import fr.eni.encheres.dal.DALException;
import fr.eni.encheres.dal.DAOFactory;
import fr.eni.encheres.dal.UtilisateurDAO;
import fr.eni.encheres.dal.jdbc.InterfaceDAO;
import fr.eni.encheres.dal.jdbc.UtilisateurDAOJdbcImpl;

public class UtilisateurManager extends Manager {
	
	private UtilisateurDAO utilisateurDAO;
	private static final String CHAMP_PSEUDO  = "pseudo";
	private static final String CHAMP_PRENOM  = "prenom";
	private static final String CHAMP_NOM  = "nom";
	private static final String CHAMP_EMAIL  = "email";
	private static final String CHAMP_TELEPHONE  = "telephone";
	private static final String CHAMP_RUE  = "rue";
	private static final String CHAMP_CODEPOSTAL  = "code_postal";
	private static final String CHAMP_VILLE  = "ville";
    private static final String CHAMP_PASS   = "motdepasse";
	private static final String CHAMP_CREDIT  = "credit";
	private static final String CHAMP_ADMIN  = "administrateur";
	

    private String resultat;
    private Map<String, String> erreurs = new HashMap<String, String>();
    
    // Pattern Singleton
    private static UtilisateurManager utilisateurManager;
    
    public static UtilisateurManager getUtilisateurManager() {
    	if(utilisateurManager == null) {
    		utilisateurManager = new UtilisateurManager();
    	}
    	return utilisateurManager;
    }
    
	private UtilisateurManager() {
		this.utilisateurDAO = DAOFactory.getUtilisateurDAO();
	}
	
	public String getResultat() {
        return resultat;
    }

    public Map<String, String> getErreurs() {
        return erreurs;
    }
	public Utilisateur connexion(HttpServletRequest request) throws BusinessException {
		
		BusinessException exception = new BusinessException();
		String pseudo = getValeurChamp( request, CHAMP_PSEUDO );
        String motDePasse = SHA256Utils.toSha256(getValeurChamp( request, CHAMP_PASS ));
        Utilisateur utilisateur;
        
		validerPseudo(pseudo, exception);
		validerMotDePasse(motDePasse, exception);
		
		if(!exception.hasErreurs())
		{
			// envoyer connexion à la bdd
			try {
				utilisateur = utilisateurDAO.connect(pseudo, motDePasse);
				return utilisateur;
			} catch (DALException e) {
				exception.ajouterErreur(e.getListeCodesErreur().get(0));
				throw exception;
			}
			
			//utilisateur = new Utilisateur(0, "admin", "Derrien", "Ronan", "test@hotmail.fr", "0606060606", "10 allée des Noisetiers", "35450", "Landavran", "8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918", 9001, true);
			
		} else 
		{
			throw exception;
		}
		
		
	}
	
	public void updateUtilisateur(HttpServletRequest request, Utilisateur utilisateur) throws DALException, BusinessException {
		
		BusinessException exception = new BusinessException();

		
		//String pseudo = getValeurChamp(request, CHAMP_PSEUDO);
		String prenom = getValeurChamp( request, CHAMP_PRENOM );
		String nom = getValeurChamp( request, CHAMP_NOM );
		String email = getValeurChamp( request, CHAMP_EMAIL );
		String telephone = getValeurChamp( request, CHAMP_TELEPHONE );
		String rue = getValeurChamp( request, CHAMP_RUE );
		String codePostal = getValeurChamp( request, CHAMP_CODEPOSTAL );
		String ville = getValeurChamp( request, CHAMP_VILLE );
		//String motDePasse = SHA256Utils.toSha256(getValeurChamp( request, CHAMP_PASS ));
		
		
		//validerPseudo(pseudo, exception);
		//validerCoherencePseudo(pseudo, exception);
		validerPrenom(prenom, exception);
		validerNom(nom, exception);
		validerEmail(email, exception);
		validerTelephone(telephone, exception);
		validerCoherenceTelephone(telephone, exception);
		validerRue(rue, exception);
		validerCodePostal(codePostal, exception);
		validerVille(ville, exception);
		//validerMotDePasse(motDePasse, exception);
		
		
		//utilisateur.setPseudo(pseudo);
		utilisateur.setPrenom(prenom);
		utilisateur.setNom(nom);
		utilisateur.setEmail(email);
		utilisateur.setTelephone(telephone);
		utilisateur.setRue(rue);
		utilisateur.setCode_postal(codePostal);
		utilisateur.setVille(ville);
		//utilisateur.setMot_de_passe(motDePasse);
		

		
		
		if(!exception.hasErreurs())
		{
			utilisateurDAO.update(utilisateur);
			System.out.println("édité");
		} else 
		{
			throw exception;
		}
		
	}
	
	public void inscription(HttpServletRequest request) throws BusinessException, DALException {
		
		BusinessException exception = new BusinessException();
		Utilisateur utilisateur;
		
		String pseudo = getValeurChamp(request, CHAMP_PSEUDO);
		String prenom = getValeurChamp( request, CHAMP_PRENOM );
		String nom = getValeurChamp( request, CHAMP_NOM );
		String email = getValeurChamp( request, CHAMP_EMAIL );
		String telephone = getValeurChamp( request, CHAMP_TELEPHONE );
		String rue = getValeurChamp( request, CHAMP_RUE );
		String codePostal = getValeurChamp( request, CHAMP_CODEPOSTAL );
		String ville = getValeurChamp( request, CHAMP_VILLE );
		String motDePasse = SHA256Utils.toSha256(getValeurChamp( request, CHAMP_PASS ));
		int credit = 0;
		boolean administrateur = false;
		
		validerPseudo(pseudo, exception);
		validerCoherencePseudo(pseudo, exception);
		validerPrenom(prenom, exception);
		validerNom(nom, exception);
		validerEmail(email, exception);
		validerTelephone(telephone, exception);
		validerCoherenceTelephone(telephone, exception);
		validerRue(rue, exception);
		validerCodePostal(codePostal, exception);
		validerVille(ville, exception);
		validerMotDePasse(motDePasse, exception);
		
		if(!exception.hasErreurs())
		{
			utilisateur = new Utilisateur(-1, pseudo, nom, prenom, email, telephone, rue, codePostal, ville, motDePasse, credit, administrateur);
			utilisateurDAO.insert(utilisateur);
			
		} else 
		{
			throw exception;
		}
		
	}
	
	public HashMap<String, String>  getInfos(int idCompte) {
		
		
		
		HashMap<String, String> userAttributes = (HashMap<String, String>) new HashMap<String, String>();
		
		Utilisateur utilisateur = new Utilisateur();
		try {
			utilisateur = utilisateurDAO.selectById(idCompte);
			
			userAttributes.put(CHAMP_PSEUDO, utilisateur.getPseudo());
			userAttributes.put(CHAMP_PRENOM, utilisateur.getPrenom());
			userAttributes.put(CHAMP_EMAIL, utilisateur.getEmail());
			userAttributes.put(CHAMP_NOM, utilisateur.getNom());
			userAttributes.put(CHAMP_TELEPHONE, utilisateur.getTelephone());
			userAttributes.put(CHAMP_RUE, utilisateur.getRue());
			userAttributes.put(CHAMP_CODEPOSTAL, utilisateur.getCode_postal());
			userAttributes.put(CHAMP_VILLE, utilisateur.getVille());
			//userAttributes.put(CHAMP_ADMIN, utilisateur.isAdministrateur() ? "Oui" : "Non" );
			
		} catch (DALException e) {
			userAttributes = null;
			e.printStackTrace();
		}
				
		return userAttributes;
	}

	public void suppression(HttpServletRequest request) throws BusinessException, DALException {
        BusinessException exception = new BusinessException();
        int noUtilisateur = (int) request.getSession().getAttribute(ATT_USER);
                
        validerNoUtilisateur(noUtilisateur, exception);
        
        if(!exception.hasErreurs())
        {
            // envoyer connexion à la bdd
            try {
                utilisateurDAO.delete(noUtilisateur);;
            } catch (DALException e) {
                exception.ajouterErreur(e.getListeCodesErreur().get(0));
                throw exception;
            }
            
            //utilisateur = new Utilisateur(0, "admin", "Derrien", "Ronan", "test@hotmail.fr", "0606060606", "10 allée des Noisetiers", "35450", "Landavran", "8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918", 9001, true);
            
        } else 
        {
            throw exception;
        }
    }

    
	private void validerNoUtilisateur(int saisieUtilisateur, BusinessException businessException) {
		if (saisieUtilisateur == 0)
			businessException.ajouterErreur(CodesResultatBLL.REGLE_UTILISATEUR_PSEUDO_ERREUR);		
	}
	
	private void validerPseudo(String saisieUtilisateur, BusinessException businessException) {
		if (saisieUtilisateur == null)
			businessException.ajouterErreur(CodesResultatBLL.REGLE_UTILISATEUR_PSEUDO_ERREUR);		
	}
	
	private void validerCoherencePseudo(String saisieUtilisateur, BusinessException businessException) {
		try {
			utilisateurDAO.selectByPseudo(saisieUtilisateur);
			businessException.ajouterErreur(CodesResultatBLL.REGLE_UTILISATEUR_PSEUDO_DEJA_EXISTANT);
		} catch (Exception e) {
			// on n'a pas trouvé d'utilisateur qui a déjà ce pseudo, on laisse l'inscription se poursuivre
		}
	}
	
	private void validerPrenom(String saisieUtilisateur, BusinessException businessException) {
		if (saisieUtilisateur == null)
			businessException.ajouterErreur(CodesResultatBLL.REGLE_UTILISATEUR_PRENOM_ERREUR);
	}
	
	private void validerNom(String saisieUtilisateur, BusinessException businessException) {
		if (saisieUtilisateur == null)
			businessException.ajouterErreur(CodesResultatBLL.REGLE_UTILISATEUR_NOM_ERREUR);
	}
	
	private void validerEmail(String saisieUtilisateur, BusinessException businessException) {
		if (saisieUtilisateur == null)
			businessException.ajouterErreur(CodesResultatBLL.REGLE_UTILISATEUR_EMAIL_ERREUR);
	}
	
	private void validerTelephone(String saisieUtilisateur, BusinessException businessException) {
		if (saisieUtilisateur == null)
			businessException.ajouterErreur(CodesResultatBLL.REGLE_UTILISATEUR_TELEPHONE_ERREUR);
	}
	
	private void validerCoherenceTelephone(String saisieUtilisateur, BusinessException businessException) {
		if (saisieUtilisateur != null && saisieUtilisateur.matches("[0-9]+") == false) // on regarde si le tel ne contient que des chiffres
			businessException.ajouterErreur(CodesResultatBLL.REGLE_UTILISATEUR_TELEPHONE_NUMERIC);
	}
	
	private void validerRue(String saisieUtilisateur, BusinessException businessException) {
		if (saisieUtilisateur == null)
			businessException.ajouterErreur(CodesResultatBLL.REGLE_UTILISATEUR_RUE_ERREUR);
	}
	
	private void validerCodePostal(String saisieUtilisateur, BusinessException businessException) {
		if (saisieUtilisateur == null)
			businessException.ajouterErreur(CodesResultatBLL.REGLE_UTILISATEUR_CODEPOSTAL_ERREUR);
	}
	
	private void validerVille(String saisieUtilisateur, BusinessException businessException) {
		if (saisieUtilisateur == null)
			businessException.ajouterErreur(CodesResultatBLL.REGLE_UTILISATEUR_VILLE_ERREUR);
	}
	
	private void validerMotDePasse(String saisieUtilisateur, BusinessException businessException) {
		if (saisieUtilisateur == null)
			businessException.ajouterErreur(CodesResultatBLL.REGLE_UTILISATEUR_MOTDEPASSE_ERREUR);
	}
	

}
