package fr.eni.encheres.bll;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import fr.eni.encheres.BusinessException;
import fr.eni.encheres.EncheresProperties;
import fr.eni.encheres.bo.ArticleVendu;
import fr.eni.encheres.bo.Categorie;
import fr.eni.encheres.bo.Enchere;
import fr.eni.encheres.bo.Retrait;
import fr.eni.encheres.bo.Utilisateur;
import fr.eni.encheres.dal.DALException;
import fr.eni.encheres.dal.DAOFactory;
import fr.eni.encheres.dal.EnchereDAO;
import fr.eni.encheres.dal.RetraitDAO;
import fr.eni.encheres.dal.UtilisateurDAO;

public class EnchereManager extends Manager {
	
	private EnchereDAO enchereDAO;

	
	
	
	
    // Pattern Singleton
    private static EnchereManager enchereManager;
    
    public static EnchereManager getEnchereManager() {
    	if(enchereManager == null) {
    		enchereManager = new EnchereManager();
    	}
    	return enchereManager;
    }
    
	private EnchereManager() {
		this.enchereDAO = DAOFactory.getEnchereDAO();
	}
	
	public int getMaxMontantEnchere(int noArticle) {
		
		
		EnchereDAO enchereDAO = DAOFactory.getEnchereDAO();
		int enchere = 0;
		
		try {
			
			enchere = enchereDAO.selectMaxEnchere(noArticle);

		} catch (Exception e) {
			enchere = 0;
			e.printStackTrace();
		}
				
		return enchere ;
	}
	
	public void insertEnchere(HttpServletRequest request) {
		
		EnchereDAO enchereDAO = DAOFactory.getEnchereDAO();
		Enchere enchere = new Enchere();
		ArticleVendu article = new ArticleVendu();
		Utilisateur utilisateur = new Utilisateur();
		int noUtilisateur = (int)request.getSession().getAttribute(EncheresProperties.getProperty("ATT_USER"));
		String noArticle = getValeurChamp(request, "no_article");
		article.setNoArticle(Integer.valueOf(noArticle));
		utilisateur.setNo_utilisateur(noUtilisateur);
		enchere.setArticle(article);
		enchere.setEncherisseur(utilisateur);
		enchere.setDateEnchere(new Date(System.currentTimeMillis()));
		int montant = Integer.valueOf(getValeurChamp(request, "prix_proposition"));
		int prixInitial = 0;
		int prixEnchere = 0;
		try {
			prixInitial = Integer.valueOf(getValeurChamp(request, "prix_initial"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			prixEnchere = Integer.valueOf(getValeurChamp(request, "prix_enchere"));	
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		enchere.setMontantEnchere(montant);
		try { 
			if(montant > prixInitial && montant > prixEnchere)
			{
				if(!enchereDAO.selectByIdArticleUutilisateur(Integer.valueOf(noArticle), noUtilisateur))
					enchereDAO.insert(enchere);
				else 
					enchereDAO.updateArticle(montant, Integer.valueOf(noArticle), noUtilisateur);
			}
			

		} catch (Exception e) {
			e.printStackTrace();
		}
				

	}
	
}
