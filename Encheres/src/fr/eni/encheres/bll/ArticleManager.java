package fr.eni.encheres.bll;


import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Locale;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import org.apache.tomcat.util.http.fileupload.IOUtils;

import fr.eni.encheres.BusinessException;
import fr.eni.encheres.bo.ArticleVendu;
import fr.eni.encheres.bo.Categorie;
import fr.eni.encheres.bo.Enchere;
import fr.eni.encheres.bo.Retrait;
import fr.eni.encheres.bo.Utilisateur;
import fr.eni.encheres.dal.ArticleDAO;
import fr.eni.encheres.dal.CategorieDAO;
import fr.eni.encheres.dal.DALException;
import fr.eni.encheres.dal.DAOFactory;
import fr.eni.encheres.dal.RetraitDAO;
import fr.eni.encheres.dal.UtilisateurDAO;


public class ArticleManager extends Manager {
	private ArticleDAO articleDAO;
	private static final String CHAMP_NOM = "nom_article";
	private static final String CHAMP_DESCRIPTION = "description";
	private static final String CHAMP_DATE_DEBUT = "date_debut_encheres";
	private static final String CHAMP_DATE_FIN = "date_fin_encheres";
	private static final String CHAMP_PRIX_INITIAL = "prix_initial";
	private static final String CHAMP_PRIX_VENTE = "prix_vente";
	private static final String CHAMP_ETAT_VENTE = "etat_vente";
	private static final String CHAMP_NO_UTILISATEUR_VENDEUR  = "no_utilisateur_vendeur";
	private static final String CHAMP_PSEUDO_VENDEUR  = "pseudo_vendeur";
	private static final String CHAMP_NO_UTILISATEUR_ACHETEUR = "no_utilisateur_acheteur";
	private static final String CHAMP_NO_CATEGORIE = "no_categorie";
	private static final String CHAMP_NOM_CATEGORIE = "categorie";
	private static final String CHAMP_RUE = "rue";
	private static final String CHAMP_VILLE = "ville";
	private static final String CHAMP_CODE_POSTAL = "code_postal";
	private static final String CHAMP_NO_RETRAIT = "no_retrait";
	private static final String CHAMP_CHEMIN_IMAGE = "chemin_image";
	
	
	// page liste encheres
	private static final String CHAMP_NO_UTILISATEUR  		= "no_utilisateur";
	private static final String CHAMP_NO_ARTICLE  			= "no_article";
	private static final String CHAMP_DATE  				= "date_enchere";
	private static final String CHAMP_MONTANT  				= "montant_enchere";
	
	private static final String CHAMP_FILTRE				= "filtre";
	private static final String CHAMP_CATEGORIE				= "select_categorie";
	private static final String CHAMP_ACHAT					= "achat_vente";
	private static final String CHAMP_ACHAT_OUVERT			= "encheres_ouvertes";
	private static final String CHAMP_ACHAT_ENCOURS			= "encheres_encours";
	private static final String CHAMP_ACHAT_WIN				= "enchere_remportees";
	private static final String CHAMP_VENTE_ENCOURS			= "ventes_encours";
	private static final String CHAMP_VENTE_NN_DEBUTEE		= "ventes_non_debutees";
	private static final String CHAMP_ACHAT_FINI			= "ventes_finies";
	
	private static ArticleManager articleManager;
	
	public static ArticleManager getArticleManager() {
		if(articleManager == null) {
			articleManager = new ArticleManager();
		}
		return articleManager;
	}
	
	public ArticleManager() {
		this.articleDAO = DAOFactory.getArticleDAO();
	}
	
	public void ajoutArticle(HttpServletRequest request) throws BusinessException {
		BusinessException exception = new BusinessException();
		ArticleVendu article;
		
		try {			
			
			String nomArticle = getValeurChamp(request, CHAMP_NOM);
			String description = getValeurChamp(request, CHAMP_DESCRIPTION);
			 
			
			
			LocalDateTime dateDebut = stringToDate(getValeurChamp(request, CHAMP_DATE_DEBUT));
			LocalDateTime dateFin =  stringToDate(getValeurChamp(request, CHAMP_DATE_FIN));
			int prixInitial = Integer.parseInt(getValeurChamp(request, CHAMP_PRIX_INITIAL));
			int prixVente = -1;
			int etatVente = -1;
			int noUtilisateurVendeur = (int)request.getAttribute(CHAMP_NO_UTILISATEUR_VENDEUR);
			Utilisateur acheteur = null;
			int noCategorie = Integer.parseInt(getValeurChamp(request, CHAMP_NO_CATEGORIE));
			
			Part imagePart = request.getPart("image");
			InputStream partInputStream = imagePart.getInputStream();
			
			
			Retrait retrait = new Retrait(-1, getValeurChamp(request, CHAMP_RUE), getValeurChamp(request, CHAMP_CODE_POSTAL), getValeurChamp(request, CHAMP_VILLE));
			RetraitDAO retraitDAO = DAOFactory.getRetraitDAO();
			
			try {
				retrait = retraitDAO.selectByFields(retrait);
			} catch (DALException e) {
				retraitDAO.insert(retrait);
			}
			

			UtilisateurDAO utilisateurDAO = DAOFactory.getUtilisateurDAO();
			Utilisateur vendeur = utilisateurDAO.selectById(noUtilisateurVendeur);
			
			CategorieDAO categorieDAO = DAOFactory.getCategorieDAO();
			Categorie categorie = categorieDAO.selectById(noCategorie);
			
			
			if(!exception.hasErreurs())
			{
				article = new ArticleVendu(-1, nomArticle, description, dateDebut, dateFin, prixInitial, prixVente, etatVente, vendeur, acheteur, categorie, retrait, partInputStream);
				ArticleDAO articleDAO = DAOFactory.getArticleDAO();
				articleDAO.insert(article);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			exception.ajouterErreur(CodesResultatBLL.REGLE_CONVERSION);
			throw exception;
		}

		
		
	}
	
	public HashMap<String, String>  getInfos(int noArticle) {
		
		
		
		HashMap<String, String> articleAttributes = (HashMap<String, String>) new HashMap<String, String>();
		
		ArticleDAO articleDAO = DAOFactory.getArticleDAO();
		ArticleVendu articleVendu = new ArticleVendu();
		
		try {
			
			articleVendu = articleDAO.selectById(noArticle);
			
			articleAttributes.put(CHAMP_NO_ARTICLE, String.valueOf(noArticle));
			articleAttributes.put(CHAMP_NOM, articleVendu.getNomArticle());
			articleAttributes.put(CHAMP_DESCRIPTION, articleVendu.getDescription());
			articleAttributes.put(CHAMP_NOM_CATEGORIE, articleVendu.getCategorie().getLibelle());
			articleAttributes.put(CHAMP_PRIX_INITIAL, String.valueOf(articleVendu.getMiseAPrix()));
			LocalDateTime dateFin = articleVendu.getDateFinEncheres();
			LocalDateTime dateDebut = articleVendu.getDateDebutEncheres();
			DateTimeFormatter dateTimeFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm").withLocale(Locale.FRANCE);
			String dateF = dateTimeFormat.format(dateFin);
			String dateD = dateTimeFormat.format(dateDebut);
			articleAttributes.put(CHAMP_DATE_FIN, dateF);
			articleAttributes.put(CHAMP_DATE_DEBUT, dateD);
			articleAttributes.put(CHAMP_NO_RETRAIT, String.valueOf(articleVendu.getAdresse().getNoRetrait()));
			articleAttributes.put(CHAMP_NO_UTILISATEUR_VENDEUR, String.valueOf(articleVendu.getVendeur().getNo_utilisateur()));
			articleAttributes.put(CHAMP_NO_UTILISATEUR_VENDEUR, articleVendu.getVendeur().getPseudo());
			
			String chemin =	noArticle + articleVendu.getVendeur().getPseudo();
			 
			try {
				Path directory = Paths.get("./");
				Path path = Files.createTempFile(directory, chemin, ".png");
				try(FileOutputStream outputStream = new FileOutputStream(path.toFile()))				
				{
					byte[] buffer = new byte[1024]; 
			        int len;
			        while ((len = articleVendu.getImageArticle().read(buffer)) != -1) { 
			            outputStream.write(buffer, 0, len); 
			        }
			        articleAttributes.put(CHAMP_CHEMIN_IMAGE, path.toString());
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				 
			
			
			
			
		} catch (DALException e) {
			articleAttributes = null;
			e.printStackTrace();
		}
				
		return articleAttributes;
	}

	private LocalDateTime stringToDate(String debut) {
		StringBuilder dateD = new StringBuilder();
		dateD.append(debut.substring(6, 10));
		dateD.append("-");
		dateD.append(debut.substring(3, 5));
		dateD.append("-");
		dateD.append(debut.substring(0, 2));
		dateD.append(debut.substring(10));
		dateD.append(":00");
		
		String retour = dateD.toString();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime date = LocalDateTime.parse(retour, formatter);
		
		return date;
		
	}
	

	
	

	public HashMap<ArticleVendu, Enchere> search(HttpServletRequest request) throws BusinessException {
		
		HashMap<ArticleVendu, Enchere> listeFinaleEncheres = new HashMap<ArticleVendu, Enchere>();
		
		try {
			List<ArticleVendu> encheres = this.articleDAO.selectAll();
			
			String filtre = getValeurChamp( request, CHAMP_FILTRE );
			
			Categorie categorie;
			try {
				categorie = DAOFactory.getCategorieDAO().selectById(Integer.parseInt(getValeurChamp(request, CHAMP_CATEGORIE)));
			} catch (Exception e) {
				categorie = null;
			}
			
			Utilisateur utilisateur;
			try {
				utilisateur = DAOFactory.getUtilisateurDAO().selectById((int) request.getSession().getAttribute(ATT_USER));
			} catch (Exception e) {
				utilisateur = null;
			}
			
			System.out.println(getValeurChamp(request, CHAMP_ACHAT_OUVERT));
			System.out.println(Boolean.parseBoolean(getValeurChamp(request, CHAMP_ACHAT_OUVERT)));
			
			boolean ach_ouvert = Boolean.parseBoolean(getValeurChamp(request, CHAMP_ACHAT_OUVERT));
			boolean ach_encours = Boolean.parseBoolean(getValeurChamp(request, CHAMP_ACHAT_ENCOURS));
			boolean ach_win = Boolean.parseBoolean(getValeurChamp(request, CHAMP_ACHAT_WIN));
			boolean ven_encours = Boolean.parseBoolean(getValeurChamp(request, CHAMP_VENTE_ENCOURS));
			boolean ven_nn_debut = Boolean.parseBoolean(getValeurChamp(request, CHAMP_VENTE_NN_DEBUTEE));
			boolean ven_fini = Boolean.parseBoolean(getValeurChamp(request, CHAMP_ACHAT_FINI));
			
			listeFinaleEncheres = articleDAO.selectWithFiltre(utilisateur, filtre, categorie, ach_ouvert, ach_encours, ach_win, ven_encours, ven_nn_debut, ven_fini);
			
		} catch (DALException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listeFinaleEncheres;
	}
}
