package fr.eni.encheres.bll;

import java.io.InputStream;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import fr.eni.encheres.BusinessException;
import fr.eni.encheres.bo.ArticleVendu;
import fr.eni.encheres.bo.Categorie;
import fr.eni.encheres.bo.Retrait;
import fr.eni.encheres.bo.Utilisateur;
import fr.eni.encheres.dal.RetraitDAO;
import fr.eni.encheres.dal.CategorieDAO;
import fr.eni.encheres.dal.DALException;
import fr.eni.encheres.dal.DAOFactory;
import fr.eni.encheres.dal.RetraitDAO;
import fr.eni.encheres.dal.UtilisateurDAO;


public class RetraitManager {
	private RetraitDAO retraitDAO;

	private static final String CHAMP_RUE = "rue";
	private static final String CHAMP_VILLE = "ville";
	private static final String CHAMP_CODE_POSTAL = "code_postal";

	
	private static RetraitManager retraitManager;
	
	public static RetraitManager getRetraitManager() {
		if(retraitManager == null) {
			retraitManager = new RetraitManager();
		}
		return retraitManager;
	}
	
	public RetraitManager() {
		this.retraitDAO = DAOFactory.getRetraitDAO();
	}
	

	
	public Retrait getRetrait(int noRetrait) {
		
		
		RetraitDAO retraitDAO = DAOFactory.getRetraitDAO();
		Retrait adresse = new Retrait();
		
		try {
			
			adresse = retraitDAO.selectById(noRetrait);

		} catch (DALException e) {
			adresse = null;
			e.printStackTrace();
		}
				
		return adresse ;
	}

	private LocalDateTime stringToDate(String debut) {
		StringBuilder dateD = new StringBuilder();
		dateD.append(debut.substring(6, 10));
		dateD.append("-");
		dateD.append(debut.substring(3, 5));
		dateD.append("-");
		dateD.append(debut.substring(0, 2));
		dateD.append(debut.substring(10));
		dateD.append(":00");
		
		String retour = dateD.toString();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime date = LocalDateTime.parse(retour, formatter);
		
		return date;
		
	}
	
	private static String getValeurChamp( HttpServletRequest request, String nomChamp ) {
        String valeur = request.getParameter( nomChamp );
        if ( valeur == null || valeur.trim().length() == 0 ) {
            return null;
        } else {
            return valeur;
        }
    }
}
