package fr.eni.encheres.bll;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;



import fr.eni.encheres.bo.Categorie;
import fr.eni.encheres.BusinessException;
import fr.eni.encheres.dal.CategorieDAO;
import fr.eni.encheres.dal.DALException;
import fr.eni.encheres.dal.DAOFactory;

public class CategorieManager {
	private CategorieDAO categorieDAO;
	private static final String CHAMP_ID = "no_categorie";
	private static final String CHAMP_LIBELLE = "libelle";
	
	private static CategorieManager categorieManager;
	
	public static CategorieManager getCategorieManager() {
		if(categorieManager == null) {
			categorieManager = new CategorieManager();
		}
		return categorieManager;
	}
	
	public CategorieManager() {
		this.categorieDAO = DAOFactory.getCategorieDAO();
	}
	
	public Map<Integer, String> selectionnerTout(HttpServletRequest request) throws BusinessException {
		BusinessException exception = new BusinessException();
		Map<Integer, String> listeRetour = new Hashtable<>();
		List<Categorie> categories; 
		
		try {
			categories = categorieDAO.selectAll();
			for (Categorie categorie : categories) {
				listeRetour.put(categorie.getNoCategorie(), categorie.getLibelle());
			}
			return listeRetour;
		} catch (DALException e) {
			exception.ajouterErreur(e.getListeCodesErreur().get(0));
			throw exception;
		}
	}
}
