package fr.eni.encheres.dal;

import java.sql.SQLException;
import java.util.List;

import fr.eni.encheres.bo.Categorie;
import fr.eni.encheres.bo.Enchere;
import fr.eni.encheres.bo.Utilisateur;
import fr.eni.encheres.dal.jdbc.AbstractDAOJdbcImpl;

public abstract class EnchereDAO extends AbstractDAOJdbcImpl<Enchere> {

	public EnchereDAO(String INSERT, String SELECT_BY_ID, String SELECT_ALL, String UPDATE, String DELETE) {
		super(INSERT, SELECT_BY_ID, SELECT_ALL, UPDATE, DELETE);
		// TODO Auto-generated constructor stub
	}

	public abstract void deleteAll() throws DALException;
	
	public abstract int selectMaxEnchere(int noArticle) throws SQLException;
	
	public abstract boolean selectByIdArticleUutilisateur(int noArticle, int noUtilisateur) throws SQLException;
	
	public abstract void updateArticle(int montant, int noArticle, int noUtilisateur) throws SQLException;
	
}
