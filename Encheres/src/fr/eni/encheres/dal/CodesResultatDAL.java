package fr.eni.encheres.dal;

/**
 * Les codes disponibles sont entre 10000 et 19999
 */
public abstract class CodesResultatDAL {
	
	/**
	 * Echec général quand tentative d'ajouter un objet null
	 */
	public static final int INSERT_OBJET_NULL=10000;
	
	/**
	 * Echec général quand erreur non gérée à l'insertion 
	 */
	public static final int INSERT_OBJET_ECHEC=10001;
	
	/**
	 * Echec de l'insertion d'un avis à cause de la note
	 */
	public static final int INSERT_UTILISATEUR_NOTE_ECHEC=10002;
	
	/**
	 * Pseudo inconnu
	 */
	public static final int SELECT_PSEUDO_ECHEC=10003;
	
	/**
	 * Combinaison pseudo/mdp inconnue
	 */
	public static final int SELECT_PSEUDO_MDP_ECHEC=10004;
	
	/**
	 * Adresse non enregistrée
	 */
	public static final int SELECT_RETRAIT_PAR_CHAMP=10005;
	
	/**
	 * Echec général quand erreur non gérée à la selection 
	 */
	public static final int SELECT_OBJET_ECHEC=10006;
}
