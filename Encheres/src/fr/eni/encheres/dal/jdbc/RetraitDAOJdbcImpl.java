package fr.eni.encheres.dal.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import fr.eni.encheres.bo.ArticleVendu;
import fr.eni.encheres.bo.Retrait;
import fr.eni.encheres.bo.Utilisateur;
import fr.eni.encheres.dal.ArticleDAO;
import fr.eni.encheres.dal.CodesResultatDAL;
import fr.eni.encheres.dal.DALException;
import fr.eni.encheres.dal.DAOFactory;
import fr.eni.encheres.dal.RetraitDAO;

public class RetraitDAOJdbcImpl extends RetraitDAO {

	private static final String INSERT = "INSERT INTO retraits (rue, code_postal, ville) VALUES (?,?,?)";
	private static final String SELECT_BY_ID = "SELECT * FROM retraits WHERE no_retrait = ?";
	private static final String SELECT_ALL = "SELECT * FROM retraits";
	private static final String UPDATE = "UPDATE retraits SET rue = ?, code_postal = ?, ville = ? WHERE no_retrait = ?";
	private static final String DELETE = "DELETE FROM retraits WHERE no_retrait = ?";
	private static final String SELECT_BY_FIELDS = "SELECT * FROM retraits WHERE rue = ? AND ville = ? AND code_postal = ?";

	public RetraitDAOJdbcImpl() {
		super(INSERT, SELECT_BY_ID, SELECT_ALL, UPDATE, DELETE);
		// TODO Auto-generated constructor stub
	}

	@Override
	public int setParameter(PreparedStatement stm, Retrait obj) throws SQLException {
		stm.setString(1, obj.getRue().trim());
		stm.setString(2, obj.getCode_postal().trim());
		stm.setString(3, obj.getVille().trim());
		return 3;
	}

	@Override
	public void manageNextKey(PreparedStatement stm, Retrait obj) throws SQLException {
		// Pour récupérer l'identity généré
		ResultSet rs = stm.getGeneratedKeys();
		if (rs.next()) {
			int id = rs.getInt(1);
			obj.setNoRetrait(id);// Mise à jour de l'attribut id
		}
	}

	@Override
	public Retrait mapping(ResultSet rs) throws SQLException {
		int no_retrait = rs.getInt("no_retrait");
		String rue = rs.getString("rue");
		String code_postal = rs.getString("code_postal");
		String ville = rs.getString("ville");

		return new Retrait(no_retrait, rue, code_postal, ville);
	}

	@Override
	public int getObjectId(Retrait obj) {
		return obj.getNoRetrait();
	}
	
	@Override
	public Retrait selectByFields(Retrait retrait) throws DALException {
		Retrait adresse = null;
		try {
			Connection con = JdbcTools.getConnection(); 
			PreparedStatement stm = con.prepareStatement(SELECT_BY_FIELDS);
			stm.setString(1, retrait.getRue());
			stm.setString(2, retrait.getVille());
			stm.setString(3, retrait.getCode_postal());
			ResultSet rs = stm.executeQuery();

			if (rs.next()) {
				adresse = mapping(rs);
			} else {
				DALException exception = new DALException();
				exception.ajouterErreur(CodesResultatDAL.SELECT_RETRAIT_PAR_CHAMP);
				throw exception;
			}

		} catch (SQLException e) {
			DALException exception = new DALException();
			exception.ajouterErreur(CodesResultatDAL.SELECT_OBJET_ECHEC);
			throw exception;
		}
		
		return adresse;
	}

}
