package fr.eni.encheres.dal.jdbc;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.sun.org.apache.xml.internal.security.utils.UnsyncBufferedOutputStream;

import fr.eni.encheres.bo.ArticleVendu;
import fr.eni.encheres.bo.Categorie;
import fr.eni.encheres.bo.Enchere;
import fr.eni.encheres.bo.Utilisateur;
import fr.eni.encheres.dal.ArticleDAO;
import fr.eni.encheres.dal.CodesResultatDAL;
import fr.eni.encheres.dal.DALException;
import fr.eni.encheres.dal.DAOFactory;
import fr.eni.encheres.dal.EnchereDAO;
import fr.eni.encheres.dal.UtilisateurDAO;

public class EnchereDAOJdbcImpl extends EnchereDAO {

	private static final String INSERT = "INSERT INTO encheres (no_utilisateur, no_article, date_enchere, montant_enchere)"
			+ "VALUES (?,?,?,?)";
	private static final String SELECT_BY_ID = "SELECT * FROM encheres WHERE no_utilisateur = ? AND no_article = ?";
	private static final String SELECT_ALL = "SELECT * FROM encheres";
	private static final String UPDATE = "UPDATE ENCHERES SET montant_enchere = ? WHERE no_utilisateur = ? AND no_article = ?";
	private static final String DELETE = null;
	private static final String DELETE_ALL = "DELETE FROM encheres";
	private static final String SELECT_MAX_PRIX = "SELECT MAX(montant_enchere) AS montant_enchere FROM encheres WHERE no_article = ?";
	
	public EnchereDAOJdbcImpl() {
		super(INSERT, SELECT_BY_ID, SELECT_ALL, UPDATE, DELETE);
		// TODO Auto-generated constructor stub
	}

	@Override
	public int setParameter(PreparedStatement stm, Enchere obj) throws SQLException {
		stm.setInt(1, obj.getEncherisseur().getNo_utilisateur());
		stm.setInt(2, obj.getArticle().getNoArticle());
		stm.setDate(3, obj.getDateEnchere());
		stm.setInt(4, obj.getMontantEnchere());
		return 4;		
	}

	@Override
	public void manageNextKey(PreparedStatement stm, Enchere user) throws SQLException {
		// rien a faire
	}

	@Override
	public Enchere mapping(ResultSet rs) throws SQLException {
		int no_utilisateur = rs.getInt("no_utilisateur");
		int no_article = rs.getInt("no_article");
		Date date_enchere = rs.getDate("date_enchere");
		int montant_enchere = rs.getInt("montant_enchere");

		Utilisateur utilisateur = new Utilisateur();
		try {
			utilisateur = DAOFactory.getUtilisateurDAO().selectById(no_utilisateur);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		ArticleVendu articleVendu = new ArticleVendu();
		try {
			articleVendu = DAOFactory.getArticleDAO().selectById(no_article);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return new Enchere(utilisateur, articleVendu, date_enchere, montant_enchere);
	}

	@Override
	public int getObjectId(Enchere obj) throws DALException {
		throw new DALException("L'objet ENCHERE ne contient pas d'id unique");
	}
	
	@Override
	public void update(Enchere obj) throws DALException {		
		throw new DALException("méthode non codé"); // TODO a faire si vraiment nécessaire
	}
	
	@Override
	public Enchere selectById(int id) throws DALException {
		throw new DALException("méthode non codé"); // TODO a faire si vraiment nécessaire
	}
	
	

	public void deleteAll() throws DALException {
		try {
			Connection con = JdbcTools.getConnection(); 
			PreparedStatement stm = con.prepareStatement(DELETE_ALL);
			int nbEnr = stm.executeUpdate();
		} catch (SQLException e) {
			throw new DALException("Erreur Technique - delete() - " + e.getMessage());
		}
	}

	/*
	@Override
	public List<Enchere> selectAll() throws DALException {
		List<Enchere> liste = new ArrayList<>();
		Enchere obj = null;
		try  {
			Connection con = JdbcTools.getConnection(); 
			Statement stm = con.createStatement();
			ResultSet rs = stm.executeQuery(SELECT_WITH_FILTRE);
			while (rs.next()) {
				int noArticle = rs.getInt("no_article");
				PreparedStatement stm2 = con.prepareStatement(REQ_TRI);
				stm2.setInt(1, noArticle);
				ResultSet rs2 = stm2.executeQuery();
				while (rs2.next()) {
					obj = this.mapping(rs2);
					liste.add(obj);
				}
			}
			return liste;

		} catch (SQLException e) {
			throw new DALException(obj.getClass().getSimpleName() + ": Erreur Technique - selectAll() - " + e.getMessage());
		}
	}
	*/
	
	@Override
	public boolean selectByIdArticleUutilisateur(int noArticle, int noUtilisateur) throws SQLException {
		int enchereMax = 0;
				Connection con = JdbcTools.getConnection();
		try {
				PreparedStatement stm = con.prepareStatement(SELECT_BY_ID);
				stm.setInt(1, noUtilisateur);
				stm.setInt(2, noArticle);
				ResultSet rs = stm.executeQuery();
				if (rs.next()) {
					return true;
				} 

			} catch (SQLException e) {
				// TODO: handle exception
			}
		return false;
	}
	
	@Override
	public void updateArticle(int montant, int noArticle, int noUtilisateur) throws SQLException {
				Connection con = JdbcTools.getConnection();
		try {
				PreparedStatement stm = con.prepareStatement(UPDATE);
				stm.setInt(1, montant);
				stm.setInt(2, noUtilisateur);
				stm.setInt(3, noArticle);
				ResultSet rs = stm.executeQuery();
				if (!rs.next()) {
					
				} 

			} catch (SQLException e) {
				// TODO: handle exception
			}
	}
	
	
	@Override
	public int selectMaxEnchere(int noArticle) throws SQLException {
		int enchereMax = 0;
				Connection con = JdbcTools.getConnection();
		try {
				PreparedStatement stm = con.prepareStatement(SELECT_MAX_PRIX);
				stm.setInt(1, noArticle);
				ResultSet rs = stm.executeQuery();
				if (rs.next()) {
					enchereMax = rs.getInt("montant_enchere");
				} 

			} catch (SQLException e) {
				// TODO: handle exception
			}
		return enchereMax;
	}
	
}
