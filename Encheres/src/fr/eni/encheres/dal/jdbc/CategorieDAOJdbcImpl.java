package fr.eni.encheres.dal.jdbc;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import fr.eni.encheres.bo.Categorie;
import fr.eni.encheres.dal.CategorieDAO;

public class CategorieDAOJdbcImpl extends CategorieDAO {

	private static final String INSERT = "INSERT INTO categories (libelle) VALUES (?)";
	private static final String SELECT_BY_ID = "SELECT * FROM categories WHERE no_categorie = ?";
	private static final String SELECT_ALL = "SELECT * FROM categories";
	private static final String UPDATE = "UPDATE categories SET libelle = ? WHERE no_categorie = ?";
	private static final String DELETE = "DELETE FROM categories WHERE no_categorie = ?";
	
	public CategorieDAOJdbcImpl() {
		super(INSERT, SELECT_BY_ID, SELECT_ALL, UPDATE, DELETE);
	}
	
	@Override
	public int setParameter(PreparedStatement stm, Categorie obj) throws SQLException {
		stm.setString(1, obj.getLibelle());
		return 1;
	}
	
	@Override
	public void manageNextKey(PreparedStatement stm, Categorie user) throws SQLException {
		// Pour récupérer l'identity généré
		ResultSet rs = stm.getGeneratedKeys();
		if (rs.next()) {
			int id = rs.getInt(1);
			user.setNoCategorie(id);// Mise à jour de l'attribut id
		}		
	}
	
	@Override
	public Categorie mapping(ResultSet rs) throws SQLException {
		int no_categorie = rs.getInt("no_categorie");
		String libelle = rs.getString("libelle");
		return new Categorie(no_categorie, libelle);
	}
	@Override
	public int getObjectId(Categorie obj) {
		return obj.getNoCategorie();
	}
	
	
}
