package fr.eni.encheres.dal.jdbc;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import fr.eni.encheres.dal.DALException;

public interface InterfaceDAO<T> {
	public int setParameter(PreparedStatement stm, T obj) throws SQLException;
	public void manageNextKey(PreparedStatement stm, T user) throws SQLException;
	public T mapping(ResultSet rs) throws SQLException;
	public int getObjectId(T obj) throws DALException;
	
	public List<T> selectAll() throws DALException;
	public void insert(T user) throws DALException;
	public T selectById(int id) throws DALException;
	public void update(T obj) throws DALException;
	public void delete(int id) throws DALException;
	
}
