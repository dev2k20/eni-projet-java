package fr.eni.encheres.dal.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.eni.encheres.dal.DALException;

public abstract class AbstractDAOJdbcImpl<T> implements InterfaceDAO<T> {
	protected String INSERT;
	protected String SELECT_BY_ID;
	protected String SELECT_ALL;
	protected String UPDATE;
	protected String DELETE;
	
	public AbstractDAOJdbcImpl(String INSERT, String SELECT_BY_ID, String SELECT_ALL, String UPDATE, String DELETE) {
		this.INSERT = INSERT;
		this.SELECT_BY_ID = SELECT_BY_ID;
		this.SELECT_ALL = SELECT_ALL;
		this.UPDATE = UPDATE;
		this.DELETE = DELETE;
	}
	
	
	public void insert(T obj) throws DALException {
		try {
			Connection con = JdbcTools.getConnection();
			PreparedStatement stm = con.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
			
			// injecter les paramètres
			this.setParameter(stm, obj);

			int nbEnr = stm.executeUpdate();
			if (nbEnr == 1) {
				this.manageNextKey(stm, obj);
			} else {
				throw new DALException(obj.getClass().getSimpleName() + ": Impossible d'enregistrer - " + obj);
			}

		} catch (SQLException e) {
			throw new DALException(obj.getClass().getSimpleName() + ": Erreur Technique - insert() - " + e.getMessage());
		}
	}
	


	public List<T> selectAll() throws DALException {
		List<T> liste = new ArrayList<>();
		T obj = null;
		try  {
			Connection con = JdbcTools.getConnection(); 
			Statement stm = con.createStatement();
			ResultSet rs = stm.executeQuery(SELECT_ALL);
			while (rs.next()) {
				obj = this.mapping(rs);
				liste.add(obj);
			}
			return liste;

		} catch (SQLException e) {
			throw new DALException(obj.getClass().getSimpleName() + ": Erreur Technique - selectAll() - " + e.getMessage());
		}
	}
	
	public T selectById(int id) throws DALException {
		try {
			Connection con = JdbcTools.getConnection(); 
			PreparedStatement stm = con.prepareStatement(SELECT_BY_ID);
			stm.setInt(1, id);
			ResultSet rs = stm.executeQuery();
			T obj = null;
			if (rs.next()) {
				obj = mapping(rs);
				return obj;
			} else {
				throw new DALException("L'article n'existe pas - id = " + id);
			}

		} catch (SQLException e) {
			throw new DALException("Erreur Technique - selectById() - " + e.getMessage());
		}
	}
	
	public void update(T obj) throws DALException {
		try {
			Connection con = JdbcTools.getConnection(); 
			PreparedStatement stm = con.prepareStatement(UPDATE);
			
			int nbParams = setParameter(stm, obj);
			
			stm.setInt(nbParams + 1, this.getObjectId(obj));
			int nbEnr = stm.executeUpdate();
			if (nbEnr != 1) {
				throw new DALException(obj.getClass().getSimpleName() + ": Impossible de mettre à jour - " + obj);
			}

		} catch (SQLException e) {
			throw new DALException(obj.getClass().getSimpleName() + ": Erreur Technique - update() - " + e.getMessage());
		}

	}
	

	public void delete(int id) throws DALException {
		try {
			Connection con = JdbcTools.getConnection(); 
			PreparedStatement stm = con.prepareStatement(DELETE);
			stm.setInt(1, id);
			int nbEnr = stm.executeUpdate();
			if (nbEnr != 1) {
				throw new DALException("Impossible de supprimer - " + id);
			}

		} catch (SQLException e) {
			throw new DALException("Erreur Technique - delete() - " + e.getMessage());
		}
	}
	

	
	
	
}
