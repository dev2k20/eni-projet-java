package fr.eni.encheres.dal.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


import fr.eni.encheres.bo.Utilisateur;
import fr.eni.encheres.dal.CodesResultatDAL;
import fr.eni.encheres.dal.DALException;
import fr.eni.encheres.dal.UtilisateurDAO;

public class UtilisateurDAOJdbcImpl extends UtilisateurDAO {
		
	private static final String INSERT = 
			"INSERT INTO Utilisateurs (pseudo, nom, prenom, email, telephone, rue, code_postal, ville, mot_de_passe,"
			+ "credit, administrateur) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
	private static final String SELECT_BY_ID = "SELECT * FROM utilisateurs WHERE no_utilisateur = ?";
	private static final String SELECT_ALL = "SELECT * FROM utilisateurs";
	private static final String UPDATE = "UPDATE utilisateurs SET pseudo = ?, nom = ?, prenom = ?, email = ?, telephone = ?,"
			+ "rue = ?, code_postal = ?, ville = ?, mot_de_passe = ?, credit = ?, administrateur = ? WHERE no_utilisateur = ?";
	private static final String DELETE = "DELETE FROM utilisateurs WHERE no_utilisateur = ?";
	
	private static final String SELECT_BY_PSEUDO = "SELECT * FROM utilisateurs WHERE pseudo = ?";
	
	public UtilisateurDAOJdbcImpl() {
		super(INSERT, SELECT_BY_ID, SELECT_ALL, UPDATE, DELETE);
	}

	@Override
	public void manageNextKey(PreparedStatement stm, Utilisateur user) throws SQLException {
		// Pour récupérer l'identity généré
		ResultSet rs = stm.getGeneratedKeys();
		if (rs.next()) {
			int id = rs.getInt(1);
			user.setNo_utilisateur(id);// Mise à jour de l'attribut id
		}		
	}
	
	
	public int setParameter(PreparedStatement stm, Utilisateur user) throws SQLException {	
		stm.setString(1, user.getPseudo());// 1 -> correspond au premier ? de la requête
		stm.setString(2, user.getNom());
		stm.setString(3, user.getPrenom());
		stm.setString(4, user.getEmail());
		stm.setString(5, user.getTelephone());
		stm.setString(6, user.getRue());
		stm.setString(7, user.getCode_postal());
		stm.setString(8, user.getVille());
		stm.setString(9, user.getMot_de_passe());
		stm.setInt(10, user.getCredit());
		stm.setBoolean(11, user.isAdministrateur());
		return 11;
	}
	
	public Utilisateur mapping(ResultSet rs) throws SQLException {
		int no_utilisateur = rs.getInt("no_utilisateur");
		String pseudo = rs.getString("pseudo");
		String nom = rs.getString("nom");
		String prenom = rs.getString("prenom");
		String email = rs.getString("email");
		String telephone = rs.getString("telephone");
		String rue = rs.getString("rue");
		String code_postal = rs.getString("code_postal");
		String ville = rs.getString("ville");
		String mot_de_passe = rs.getString("mot_de_passe");
		int credit = rs.getInt("credit");
		boolean administrateur = rs.getBoolean("administrateur");
		
		return new Utilisateur(no_utilisateur, pseudo, nom, prenom, email, telephone, rue, code_postal, ville, mot_de_passe, credit, administrateur);
	}

	public int getObjectId(Utilisateur user) {
		return user.getNo_utilisateur();
	}

	@Override
	public Utilisateur selectByPseudo(String pseudo) throws DALException {
		Utilisateur user = new Utilisateur();
		try {
			Connection con = JdbcTools.getConnection(); 
			PreparedStatement stm = con.prepareStatement(SELECT_BY_PSEUDO);
			stm.setString(1, pseudo);
			ResultSet rs = stm.executeQuery();

			if (rs.next()) {
				user = mapping(rs);
			} else {
				DALException exception = new DALException();
				exception.ajouterErreur(CodesResultatDAL.SELECT_PSEUDO_MDP_ECHEC);
				throw exception;
			}

		} catch (SQLException e) {
			DALException exception = new DALException();
			exception.ajouterErreur(CodesResultatDAL.INSERT_OBJET_ECHEC);
			throw exception;
		}
		
		return user;
	}
	
	@Override
	public Utilisateur connect(String login, String password) throws DALException {
		Utilisateur user = this.selectByPseudo(login);
		
		if (password.equals(user.getMot_de_passe())) {
			return user;
		} else {
			DALException exception = new DALException();
			exception.ajouterErreur(CodesResultatDAL.SELECT_PSEUDO_MDP_ECHEC);
			throw exception;
		}		
	}




}
