package fr.eni.encheres.dal.jdbc;
/*
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
*/
import fr.eni.encheres.dal.Settings;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JdbcTools {
    private static String urlDB;
    private static String userDB;
    private static String pwdDB;

    static {
        urlDB = Settings.getProperty("url");
        userDB = Settings.getProperty("user");
        pwdDB = Settings.getProperty("pwd");
    }

    private static Connection con;

    public static Connection getConnection() throws SQLException {
        if (con == null || con.isClosed()) {
        	try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            con = DriverManager.getConnection(urlDB, userDB, pwdDB);
        }
        return con;
    }

    public static void close() throws SQLException {
        if (con != null) {
            con.close();
        }
    }
}
