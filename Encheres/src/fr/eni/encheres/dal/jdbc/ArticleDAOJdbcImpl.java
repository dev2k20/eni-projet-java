package fr.eni.encheres.dal.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.eclipse.jdt.internal.compiler.ast.TrueLiteral;

import fr.eni.encheres.bo.ArticleVendu;
import fr.eni.encheres.bo.Categorie;
import fr.eni.encheres.bo.Enchere;
import fr.eni.encheres.bo.Retrait;
import fr.eni.encheres.bo.Utilisateur;
import fr.eni.encheres.dal.ArticleDAO;
import fr.eni.encheres.dal.CategorieDAO;
import fr.eni.encheres.dal.DALException;
import fr.eni.encheres.dal.DAOFactory;
import fr.eni.encheres.dal.RetraitDAO;
import fr.eni.encheres.dal.UtilisateurDAO;

public class ArticleDAOJdbcImpl extends ArticleDAO {

	private static final String INSERT = "INSERT INTO articles_vendus (nom_article, description, date_debut_encheres, date_fin_encheres,"
			+ "prix_initial, prix_vente, etat_vente, no_utilisateur_vendeur, no_utilisateur_acheteur, no_categorie, no_retrait, image) "
			+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
	private static final String SELECT_BY_ID = "SELECT * FROM articles_vendus WHERE no_article = ?";
	private static final String SELECT_ALL = "SELECT * FROM articles_vendus";
	private static final String UPDATE = "UPDATE articles_vendus SET nom_article = ?, description = ?, date_debut_encheres = ?,"
			+ "date_fin_encheres = ?, prix_initial = ?, prix_vente = ?, etat_vente = ?, no_utilisateur_vendeur = ?,"
			+ "no_utilisateur_acheteur = ?, no_categorie = ?, no_retrait = ? WHERE no_article = ?";
	private static final String DELETE = "DELETE FROM articles_vendus WHERE no_article = ?";

	/*private static final String SELECT_WITH_FILTRE = "select distinct Encheres.no_article \n"
		+ "from encheres \n"
		+ "inner join ARTICLES_VENDUS article on encheres.no_article = article.no_article \n"
		+ "WHERE '1' = '1' \n";*/
	private static final String SELECT_WITH_FILTRE =
		"select DISTINCT article.* \n" + 
		"from ARTICLES_VENDUS  as article \n" + 
		"left join Encheres encheres on encheres.no_article = article.no_article \n" + 
		"WHERE '1' = '1' \n";
		
	private static final String REQ_TRI = "select TOP 1 encheres.* \n" + 
		"from encheres \n " +
		"where encheres.no_article = ? \n " + 
		"order by montant_enchere desc";
		
	
	
	public ArticleDAOJdbcImpl() {
		super(INSERT, SELECT_BY_ID, SELECT_ALL, UPDATE, DELETE);
		// TODO Auto-generated constructor stub
	}

	@Override
	public int setParameter(PreparedStatement stm, ArticleVendu obj) throws SQLException {
		stm.setString(1, obj.getNomArticle());
		stm.setString(2, obj.getDescription());
		stm.setTimestamp(3, Timestamp.valueOf(obj.getDateDebutEncheres()));
		stm.setTimestamp(4, Timestamp.valueOf(obj.getDateFinEncheres()));
		stm.setInt(5, obj.getMiseAPrix());
		stm.setString(6, null);
		stm.setInt(7, 1);
		stm.setInt(8, obj.getVendeur().getNo_utilisateur());
		stm.setString(9, null);
		stm.setInt(10, obj.getCategorie().getNoCategorie());
		stm.setInt(11, obj.getAdresse().getNoRetrait());
		stm.setBlob(12, obj.getImageArticle());
		return 12;		
	}

	@Override
	public void manageNextKey(PreparedStatement stm, ArticleVendu obj) throws SQLException {
		// Pour récupérer l'identity généré
		ResultSet rs = stm.getGeneratedKeys();
		if (rs.next()) {
			int id = rs.getInt(1);
			obj.setNoArticle(id);// Mise à jour de l'attribut id
		}		
	}

	@Override
	public ArticleVendu mapping(ResultSet rs) throws SQLException {
		int no_article 				= rs.getInt("no_article");
		String nom_article 			= rs.getString("nom_article");
		String description 			= rs.getString("description");
		LocalDateTime date_debut_encheres 	= rs.getTimestamp("date_debut_encheres").toLocalDateTime();
		LocalDateTime date_fin_encheres 	= rs.getTimestamp("date_fin_encheres").toLocalDateTime();
		int prix_initial 			= rs.getInt("prix_initial");
		int prix_vente 				= rs.getInt("prix_vente");
		int etat_vente 				= rs.getInt("etat_vente");
		int no_utilisateur_vendeur 	= rs.getInt("no_utilisateur_vendeur");
		int no_utilisateur_acheteur = rs.getInt("no_utilisateur_acheteur");
		int no_categorie 			= rs.getInt("no_categorie");
		int no_retrait	 			= rs.getInt("no_retrait");
		InputStream imageArticle 	= rs.getBlob("image").getBinaryStream(); 
		
		
		UtilisateurDAO utilisateurDao = DAOFactory.getUtilisateurDAO();
		Utilisateur vendeur = new Utilisateur();
		try {
			vendeur = utilisateurDao.selectById(no_utilisateur_vendeur);
		} catch (Exception e) {
			System.out.println(e);
		}
		
		Utilisateur acheteur = new Utilisateur();
		try {
			acheteur = utilisateurDao.selectById(no_utilisateur_acheteur);
		} catch (DALException e) {
			System.out.println(e);
		}
		
		
		CategorieDAO categorieDao = DAOFactory.getCategorieDAO();
		Categorie categorie = new Categorie();
		try {
			categorie = categorieDao.selectById(no_categorie);
		} catch (Exception e) {
			System.out.println(e);
		}
		
		
		
		RetraitDAO retraitDao = DAOFactory.getRetraitDAO();
		Retrait retrait = new Retrait();
		try {
			retrait = retraitDao.selectById(no_retrait);
		} catch (Exception e) {
			System.out.println(e);
		}
		

		return new ArticleVendu(no_article, nom_article, description, date_debut_encheres, date_fin_encheres, 
				prix_initial, prix_vente, etat_vente, vendeur, acheteur, categorie, retrait, imageArticle);
		
	}
	
	@Override
	public int getObjectId(ArticleVendu obj) {
		return obj.getNoArticle();
	}

	@Override
	public Enchere getLastEnchere(ArticleVendu obj) throws SQLException {
		String req = 
				"select TOP 1 encheres.* \n" 			+ 
				"from encheres \n " 					+
				"where encheres.no_article = ? \n "		+ 
				"order by montant_enchere desc";
		Enchere enchere = null;
		try {
			Connection con = JdbcTools.getConnection(); 
			PreparedStatement stm = con.prepareStatement(req);
			stm.setInt(1, obj.getNoArticle());
			ResultSet rs = stm.executeQuery();
			
			while (rs.next()) {
				enchere = DAOFactory.getEnchereDAO().mapping(rs);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return enchere;
	}
	

	
	@Override
	public HashMap<ArticleVendu, Enchere> selectWithFiltre(Utilisateur utilisateur, String filtre, Categorie categorie, boolean ach_ouvert,
			boolean ach_encours, boolean ach_win, boolean ven_encours, boolean ven_nn_debut, boolean ven_fini) throws DALException {
		
		try {
			List<ArticleVendu> liste = new ArrayList<ArticleVendu>();
			
			Connection con = JdbcTools.getConnection(); 
			
			
			
			String req = "";
			int count = 0; // sert a savoir si on met des UNION ou non
			
			
			String SELECT_FINAL = SELECT_WITH_FILTRE;
			

			
			
			if (filtre != null) {
				SELECT_FINAL += "AND LOWER(article.nom_article) LIKE (?) \n ";				
			}
			if (categorie != null) {
				SELECT_FINAL += "AND article.no_categorie = " + categorie.getNoCategorie() + " \n ";
 			}
			
			
			req = SELECT_FINAL;
			req += "AND article.no_article in ( \n ";
			
			String BASE = 
					"	--base \n" +
					"	select article.no_article \n" + 
					"	from ARTICLES_VENDUS  as article \n" + 
					"	left join Encheres encheres on encheres.no_article = article.no_article \n" + 
					"	WHERE '1' = '1' ";
			if (utilisateur != null) {
				if (ach_ouvert) {
					req+= 	"	(select no_article from ARTICLES_VENDUS where etat_vente = (select no_etat from etat where libelle = 'Créée') ) \n" + 
							"	union \n";
				}
				
				if (ach_encours) {
					req +=  "	(select article.no_article from ARTICLES_VENDUS as article \n" + 
							"	inner join Encheres encheres on encheres.no_article = article.no_article \n" + 
							"	where encheres.no_utilisateur = " + utilisateur.getNo_utilisateur() + 	"\n" +
							"	and etat_vente = (select no_etat from etat where libelle = 'En cours') ) \n" + 
							"	union \n";
				}
				
				if (ach_win) {
					req +=  "	(select no_article from ARTICLES_VENDUS as article \n" + 
							"	where no_utilisateur_acheteur = " + utilisateur.getNo_utilisateur() + "\n" +
							"	union \n";
				}
				
				if (ven_encours) {
					req +=  "	(select no_article from ARTICLES_VENDUS as article	\n" + 
							"	where no_utilisateur_vendeur = " + utilisateur.getNo_utilisateur() + 	"\n" +
							"	and etat_vente = (select no_etat from etat where libelle = 'En cours') ) \n" + 
							"	union \n";
				}
				
				if (ven_nn_debut) {
					req +=  "	(select no_article from ARTICLES_VENDUS as article	\n" + 
							"	where no_utilisateur_vendeur = " + utilisateur.getNo_utilisateur() + 	"\n" +
							"	and etat_vente = (select no_etat from etat where libelle = 'Créée') ) \n" + 
							"	union \n";
				}
				
				if (ven_fini) {
					req +=  "	(select no_article from ARTICLES_VENDUS as article	\n" + 
							"	where no_utilisateur_vendeur = " + utilisateur.getNo_utilisateur() + 	"\n" +
							"	and etat_vente = (select no_etat from etat where libelle = 'Enchères terminées') ) \n" + 
							"	union \n";
				}
			}
			

			

			
			if ((!ach_ouvert && !ach_encours && !ach_win) && (!ven_encours && !ven_nn_debut && !ven_fini)) {
				//if (count > 0) req += " UNION ";
				req += " ("+ BASE + ") ";
				req += " \n UNION \n";
				count++;
			}
			
			req += " (select null)" + "\n)";
			
			
			
			
			System.out.println(req);
			
			PreparedStatement stm = con.prepareStatement(req);
			
			if (filtre != null)  stm.setString(1, "%"+filtre+"%");
			/*if (filtre != null) {
				for (int i = 1; i <= count; i++) {
					stm.setString(i, "%"+filtre+"%");
				}				
			}*/
			
			
			ResultSet rs = stm.executeQuery();
	
			
			HashMap<ArticleVendu, Enchere> list2 = new HashMap<ArticleVendu, Enchere>();
			
			
			while (rs.next()) {
				ArticleVendu articleVendu = DAOFactory.getArticleDAO().mapping(rs);
				System.out.println(articleVendu);
				Enchere enchere = DAOFactory.getArticleDAO().getLastEnchere(articleVendu);
				//System.out.println(enchere);
				liste.add(articleVendu);
				
				list2.put(articleVendu, enchere);
				
				list2.forEach((k,v) -> {
					System.out.println("key: "+k+" value:"+v);
				});
				
				for(Entry<ArticleVendu, Enchere> h : list2.entrySet()) {
					
				}
				
				//liste.add(enchere);
				
				/*PreparedStatement stm2 = con.prepareStatement(REQ_TRI);
				stm2.setInt(1, noArticle);
				ResultSet rs2 = stm2.executeQuery();
				while (rs2.next()) {
					Enchere obj = this.mapping(rs2);
					liste.add(obj);
				}*/
			}
			

			return list2;

		} catch (SQLException e) {
			throw new DALException("Erreur Technique - selectWithFiltre() - " + e.getMessage());
		}
		
	}
	

}
