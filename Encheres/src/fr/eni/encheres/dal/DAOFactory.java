package fr.eni.encheres.dal;

import fr.eni.encheres.dal.jdbc.ArticleDAOJdbcImpl;
import fr.eni.encheres.dal.jdbc.CategorieDAOJdbcImpl;
import fr.eni.encheres.dal.jdbc.EnchereDAOJdbcImpl;
import fr.eni.encheres.dal.jdbc.RetraitDAOJdbcImpl;
import fr.eni.encheres.dal.jdbc.UtilisateurDAOJdbcImpl;

/**
 * Design Pattern Factory
 * -- Fabrique d'instance 
 * -- Centralise les instances de la couche DAL
 * @author ENI Ecole
 *
 */
public class DAOFactory {
	//Instance d'ArticleDAO
	
	public static UtilisateurDAOJdbcImpl getUtilisateurDAO() {
		return new UtilisateurDAOJdbcImpl();
	}
	
	public static CategorieDAOJdbcImpl getCategorieDAO() {
		return new CategorieDAOJdbcImpl();
	}
	
	public static RetraitDAOJdbcImpl getRetraitDAO() {
		return new RetraitDAOJdbcImpl();
	}
	
	public static ArticleDAOJdbcImpl getArticleDAO() {
		return new ArticleDAOJdbcImpl();
	}
	
	public static EnchereDAOJdbcImpl getEnchereDAO() {
		return new EnchereDAOJdbcImpl();
	}
}
