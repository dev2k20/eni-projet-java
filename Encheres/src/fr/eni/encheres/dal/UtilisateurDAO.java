package fr.eni.encheres.dal;

import fr.eni.encheres.bo.Utilisateur;
import fr.eni.encheres.dal.jdbc.AbstractDAOJdbcImpl;

public abstract class UtilisateurDAO extends AbstractDAOJdbcImpl<Utilisateur>{

	public UtilisateurDAO(String INSERT, String SELECT_BY_ID, String SELECT_ALL, String UPDATE, String DELETE) {
		super(INSERT, SELECT_BY_ID, SELECT_ALL, UPDATE, DELETE);
		// TODO Auto-generated constructor stub
	}
	
	public abstract Utilisateur selectByPseudo(String pseudo) throws DALException;
	
	public abstract Utilisateur connect(String login, String password) throws DALException;

}
