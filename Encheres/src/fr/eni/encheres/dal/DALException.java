package fr.eni.encheres.dal;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class DALException extends Exception {

	
	private static final long serialVersionUID = 1L;
	private List<Integer> listeCodesErreur;
	
	public DALException() {
		super();
		this.listeCodesErreur=new ArrayList<>();
	}

    public DALException(String message) {
        super(message);
    }

    public DALException(Throwable cause) {
        super(cause);
    }

    public DALException(String message, Throwable cause) {
        super(message, cause);
    }

    public DALException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
    
    public void ajouterErreur(int code)
	{
		if(!this.listeCodesErreur.contains(code))
		{
			this.listeCodesErreur.add(code);
		}
	}
    
    public List<Integer> getListeCodesErreur()
	{
		return this.listeCodesErreur;
	}

}
