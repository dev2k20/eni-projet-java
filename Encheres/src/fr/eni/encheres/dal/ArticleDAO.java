package fr.eni.encheres.dal;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import fr.eni.encheres.bo.ArticleVendu;
import fr.eni.encheres.bo.Categorie;
import fr.eni.encheres.bo.Enchere;
import fr.eni.encheres.bo.Utilisateur;
import fr.eni.encheres.dal.jdbc.AbstractDAOJdbcImpl;
import fr.eni.encheres.dal.jdbc.ArticleDAOJdbcImpl;
import fr.eni.encheres.dal.jdbc.InterfaceDAO;

public abstract class ArticleDAO extends AbstractDAOJdbcImpl<ArticleVendu> {

	public ArticleDAO(String INSERT, String SELECT_BY_ID, String SELECT_ALL, String UPDATE, String DELETE) {
		super(INSERT, SELECT_BY_ID, SELECT_ALL, UPDATE, DELETE);
	}
	
	public abstract HashMap<ArticleVendu, Enchere> selectWithFiltre(Utilisateur utilisateur, String filtre, Categorie categorie, boolean ach_ouvert, 
			boolean ach_encours, boolean ach_win, boolean ven_encours, boolean ven_nn_debut, boolean ven_fini) throws DALException;
	
	
	public abstract Enchere getLastEnchere(ArticleVendu obj) throws SQLException;
	
}
