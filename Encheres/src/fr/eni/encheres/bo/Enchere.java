package fr.eni.encheres.bo;

import java.sql.Date;

public class Enchere {
	private Utilisateur encherisseur;
	private ArticleVendu article;
	private Date dateEnchere;
	private int montantEnchere;
	public Utilisateur getEncherisseur() {
		return encherisseur;
	}
	public void setEncherisseur(Utilisateur encherisseur) {
		this.encherisseur = encherisseur;
	}
	public ArticleVendu getArticle() {
		return article;
	}
	public void setArticle(ArticleVendu article) {
		this.article = article;
	}
	public Date getDateEnchere() {
		return dateEnchere;
	}
	public void setDateEnchere(Date dateEnchere) {
		this.dateEnchere = dateEnchere;
	}
	public int getMontantEnchere() {
		return montantEnchere;
	}
	public void setMontantEnchere(int montantEnchere) {
		this.montantEnchere = montantEnchere;
	}
	
	public Enchere() {
		super();
	}
	
	public Enchere(Utilisateur encherisseur, ArticleVendu article, Date dateEnchere, int montantEnchere) {
		super();
		this.encherisseur = encherisseur;
		this.article = article;
		this.dateEnchere = dateEnchere;
		this.montantEnchere = montantEnchere;
	}
	
	@Override
	public String toString() {
		return "Enchere [encherisseur=" + encherisseur + ", article=" + article + ", dateEnchere=" + dateEnchere
				+ ", montantEnchere=" + montantEnchere + "]";
	}
	
	
}
