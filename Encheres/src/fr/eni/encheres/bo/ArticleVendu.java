package fr.eni.encheres.bo;

import java.io.InputStream;
import java.time.LocalDateTime;

public class ArticleVendu {
	private int noArticle; 
	private String nomArticle; 
	private String description; 
	private LocalDateTime dateDebutEncheres; 
	private LocalDateTime dateFinEncheres; 
	private int miseAPrix; 
	private int prixVente; 
	private int etatVente;
	private Utilisateur acheteur;
	private Utilisateur vendeur;
	private Categorie categorieArticle;
	private Retrait lieuRetrait;
	private InputStream imageArticle;
	
	public int getNoArticle() {
		return noArticle;
	}
	public void setNoArticle(int noArticle) {
		this.noArticle = noArticle;
	}
	public String getNomArticle() {
		return nomArticle;
	}
	public void setNomArticle(String nomArticle) {
		this.nomArticle = nomArticle;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public LocalDateTime getDateDebutEncheres() {
		return dateDebutEncheres;
	}
	public void setDateDebutEncheres(LocalDateTime dateDebutEncheres) {
		this.dateDebutEncheres = dateDebutEncheres;
	}
	public LocalDateTime getDateFinEncheres() {
		return dateFinEncheres;
	}
	public void setDateFinEncheres(LocalDateTime dateFinEncheres) {
		this.dateFinEncheres = dateFinEncheres;
	}
	public int getMiseAPrix() {
		return miseAPrix;
	}
	public void setMiseAPrix(int miseAPrix) {
		this.miseAPrix = miseAPrix;
	}
	public int getPrixVente() {
		return prixVente;
	}
	public void setPrixVente(int prixVente) {
		this.prixVente = prixVente;
	}
	public int getEtatVente() {
		return etatVente;
	}
	public void setEtatVente(int etatVente) {
		this.etatVente = etatVente;
	}
	public Utilisateur getAcheteur() {
		return acheteur;
	}
	public void setAcheteur(Utilisateur acheteur) {
		this.acheteur = acheteur;
	}
	public Utilisateur getVendeur() {
		return vendeur;
	}
	public void setVendeur(Utilisateur vendeur) {
		this.vendeur = vendeur;
	}
	public Categorie getCategorie() {
		return categorieArticle;
	}
	public void setCategorie(Categorie categorie) {
		this.categorieArticle = categorie;
	}
	public Retrait getAdresse() {
		return lieuRetrait;
	}
	public void setAdresse(Retrait adresse) {
		this.lieuRetrait = adresse;
	}
	public InputStream getImageArticle() {
		return imageArticle;
	}
	public void setImageArticle(InputStream imageArticle) {
		this.imageArticle = imageArticle;
	}
	public ArticleVendu() {
		super();
	}
	public ArticleVendu(int noArticle, String nomArticle, String description, LocalDateTime dateDebutEncheres,
			LocalDateTime dateFinEncheres, int miseAPrix, int prixVente, int etatVente, Utilisateur vendeur, Utilisateur acheteur, Categorie noCategorie, Retrait adresse, InputStream image) {
		super();
		this.noArticle = noArticle;
		this.nomArticle = nomArticle;
		this.description = description;
		this.dateDebutEncheres = dateDebutEncheres;
		this.dateFinEncheres = dateFinEncheres;
		this.miseAPrix = miseAPrix;
		this.prixVente = prixVente;
		this.etatVente = etatVente;
		this.acheteur = acheteur;
		this.vendeur = vendeur;
		this.categorieArticle = noCategorie;
		this.lieuRetrait = adresse;
		this.imageArticle = image;
	}
	@Override
	public String toString() {
		return "ArticleVendu [noArticle=" + noArticle + ", nomArticle=" + nomArticle + ", description=" + description
				+ ", dateDebutEncheres=" + dateDebutEncheres + ", dateFinEncheres=" + dateFinEncheres + ", miseAPrix="
				+ miseAPrix + ", prixVente=" + prixVente + ", etatVente=" + etatVente + ", acheteur=" + acheteur
				+ ", vendeur=" + vendeur + ", noCategorie=" + categorieArticle + ", adresse=" + lieuRetrait + ", image=" + imageArticle +"]";
	}
	
	
	
}
