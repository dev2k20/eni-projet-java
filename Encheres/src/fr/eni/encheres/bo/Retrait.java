package fr.eni.encheres.bo;

public class Retrait {
	private int no_retrait;
	private String rue;
	private String codePostal;
	private String ville;
	
	public int getNoRetrait( ) {
		return no_retrait;
	}	
	public void setNoRetrait(int no_retrait) {
		this.no_retrait = no_retrait;
	}	
	public String getRue() {
		return rue;
	}
	public void setRue(String rue) {
		this.rue = rue;
	}
	public String getCode_postal() {
		return codePostal;
	}
	public void setCode_postal(String code_postal) {
		this.codePostal = code_postal;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	public Retrait() {
		super();
	}
	public Retrait(int no_retrait, String rue, String code_postal, String ville) {
		super();
		this.no_retrait = no_retrait;
		this.rue = rue;
		this.codePostal = code_postal;
		this.ville = ville;
	}
	@Override
	public String toString() {
		return "Retrait [no_retrait= "+no_retrait+", rue=" + rue + ", code_postal=" + codePostal + ", ville=" + ville + "]";
	}
	
		
}
