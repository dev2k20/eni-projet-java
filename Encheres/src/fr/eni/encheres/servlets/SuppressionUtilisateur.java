package fr.eni.encheres.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eni.encheres.BusinessException;
import fr.eni.encheres.EncheresProperties;
import fr.eni.encheres.bll.UtilisateurManager;

/**
 * Servlet implementation class SuppressionUtilisateur
 */
@WebServlet("/suppressionUtilisateur")
public class SuppressionUtilisateur extends HttpServlet {
	private static final long serialVersionUID = 1L;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SuppressionUtilisateur() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
		//this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UtilisateurManager form = UtilisateurManager.getUtilisateurManager();

        /* Récupération de la session depuis la requête */
        HttpSession session = request.getSession();
        
        if (session.getAttribute(EncheresProperties.getProperty("ATT_USER")) == null)
        	response.sendRedirect( request.getContextPath() +  EncheresProperties.getProperty("VUE_ACCUEIL"));
        	//this.getServletContext().getRequestDispatcher( VUE_ACCUEIL ).forward( request, response );	
        else {

    		try {
    			form.suppression(request);
    			
    			/**
    	         * Si aucune erreur de validation n'a eu lieu, alors ajout du bean
    	         * Utilisateur à la session, sinon suppression du bean de la session.
    	         */

    			session.setAttribute( EncheresProperties.getProperty("ATT_SESSION_USER"), null );
    			session.setAttribute( EncheresProperties.getProperty("ATT_USER"), null );
        		response.sendRedirect( request.getContextPath() +  EncheresProperties.getProperty("VUE_ACCUEIL"));

    		} catch (BusinessException e) {
    			request.setAttribute("listeCodesErreur", e.getListeCodesErreur());
    		} catch (Exception e) {
    		}
    		if(session.getAttribute(EncheresProperties.getProperty("ATT_SESSION_USER")) != null)
    			response.sendRedirect(request.getHeader("referer"));
            //this.getServletContext().getRequestDispatcher( pageSuivante ).forward( request, response );	
        }
        	
	}

}
