package fr.eni.encheres.servlets;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.eni.encheres.BusinessException;
import fr.eni.encheres.EncheresProperties;
import fr.eni.encheres.bll.ArticleManager;
import fr.eni.encheres.bll.CategorieManager;
import fr.eni.encheres.bll.EnchereManager;
import fr.eni.encheres.bll.RetraitManager;
import fr.eni.encheres.bll.UtilisateurManager;
import fr.eni.encheres.bo.Enchere;
import fr.eni.encheres.bo.Retrait;

/**
 * Servlet implementation class DetailArticle
 */
@WebServlet("/detailArticle")
public class DetailArticle extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String VUE = "/WEB-INF/detailArticle.jsp";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DetailArticle() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 /* Affichage de la page de détail de vente ou d'accueil si l'utilisateur est connecté ou non */
    	Object id = request.getSession().getAttribute(EncheresProperties.getProperty("ATT_USER"));
    	if (id != null)
    	{
    		ArticleManager articleManager = ArticleManager.getArticleManager();
    		RetraitManager retraitManager = RetraitManager.getRetraitManager();
    		EnchereManager enchereManager = EnchereManager.getEnchereManager();

    		try {
    			//Map<String, String> article = articleManager.getInfos(1);
    			Map<String, String> article = articleManager.getInfos(Integer.valueOf(request.getParameter("noArticle")));
    			Retrait adresse = retraitManager.getRetrait(Integer.valueOf(article.get("no_retrait")));
    			int enchere = enchereManager.getMaxMontantEnchere(Integer.valueOf(article.get("no_article")));
    			request.setAttribute("prix_enchere", String.valueOf(enchere));
    			request.setAttribute("no_article", article.get("no_article"));
    			request.setAttribute("nom_article", article.get("nom_article"));
    			request.setAttribute("description", article.get("description"));
    			request.setAttribute("categorie", article.get("categorie"));
    			request.setAttribute("prix_initial", article.get("prix_initial"));
    			request.setAttribute("date_fin_encheres", article.get("date_fin_encheres"));
    			request.setAttribute("date_debut_encheres", article.get("date_debut_encheres"));
    			request.setAttribute("rue", adresse.getRue());
    			request.setAttribute("ville", adresse.getVille());
    			request.setAttribute("code_postal", adresse.getCode_postal());
    			request.setAttribute("pseudo", request.getSession().getAttribute(EncheresProperties.getProperty("ATT_SESSION_USER")));
    			request.setAttribute("chemin_image", article.get("chemin_image"));

			} catch (Exception e) {
				//request.setAttribute("listeCodesErreur", e.getListeCodesErreur());
				this.getServletContext().getRequestDispatcher( EncheresProperties.getProperty("VUE_ACCUEIL") ).forward( request, response );
				return;
			}
    		
        	this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );    
        	
    	}
    	else 
    		response.sendRedirect( request.getContextPath() +  EncheresProperties.getProperty("VUE_ACCUEIL"));
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		EnchereManager enchereManager = EnchereManager.getEnchereManager();
		
		enchereManager.insertEnchere(request);
		
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
