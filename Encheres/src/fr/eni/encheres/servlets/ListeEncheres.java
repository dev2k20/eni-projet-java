package fr.eni.encheres.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.eni.encheres.BusinessException;
import fr.eni.encheres.bll.ArticleManager;
import fr.eni.encheres.bll.EnchereManager;
import fr.eni.encheres.bo.ArticleVendu;
import fr.eni.encheres.bo.Categorie;
import fr.eni.encheres.bo.Enchere;
import fr.eni.encheres.dal.DAOFactory;

/**
 * Servlet implementation class ListeEncheres
 */
@WebServlet("/listeEncheres")
public class ListeEncheres extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	public static final String VUE = "/WEB-INF/listeEncheres.jsp";
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HashMap<ArticleVendu, Enchere> list = new HashMap<ArticleVendu, Enchere>();
		List<Categorie> categories = new ArrayList<>();
		try {
			list = DAOFactory.getArticleDAO().selectWithFiltre(null, null, null, false, false, false, false, false, false);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			categories = DAOFactory.getCategorieDAO().selectAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		request.setAttribute("arrayEncheres", list);
		request.setAttribute("categories", categories);
		this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ArticleManager articleManager = ArticleManager.getArticleManager();
		HashMap<ArticleVendu, Enchere> list = new HashMap<ArticleVendu, Enchere>();
		List<Categorie> categories = new ArrayList<>();
		try {
			
			list = articleManager.search(request);
		} catch (BusinessException e) {
			request.setAttribute("listeCodesErreur", e.getListeCodesErreur());
			this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
			return;
		}
		
		try {
			categories = DAOFactory.getCategorieDAO().selectAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		request.setAttribute("arrayEncheres", list);
		request.setAttribute("categories", categories);
		this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
	}

}
