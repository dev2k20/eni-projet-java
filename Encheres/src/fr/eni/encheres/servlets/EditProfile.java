package fr.eni.encheres.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eni.encheres.BusinessException;
import fr.eni.encheres.EncheresProperties;
import fr.eni.encheres.bll.CodesResultatBLL;
import fr.eni.encheres.bll.UtilisateurManager;
import fr.eni.encheres.bo.Utilisateur;
import fr.eni.encheres.dal.DALException;
import fr.eni.encheres.dal.DAOFactory;
import fr.eni.encheres.dal.UtilisateurDAO;

/**
 * Servlet implementation class EditProfile
 */
@WebServlet("/editProfile")
public class EditProfile extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public static final String ATT_USER_INFOS   = "userInfos";
    public static final String VUE              = "/WEB-INF/editProfile.jsp";
       
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if (request.getSession().getAttribute(EncheresProperties.getProperty("ATT_USER")) == null ) {
			response.sendRedirect(request.getContextPath() + "/accueil");
		} else {
			UtilisateurManager manager = UtilisateurManager.getUtilisateurManager();
			request.setAttribute( ATT_USER_INFOS, manager.getInfos((int) request.getSession().getAttribute(EncheresProperties.getProperty("ATT_USER"))) );
			this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );	
		}	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		int id = (int) session.getAttribute(EncheresProperties.getProperty("ATT_USER"));
		Utilisateur utilisateur = new Utilisateur();
		
		UtilisateurDAO userDao = DAOFactory.getUtilisateurDAO();
		try {
			utilisateur = userDao.selectById(id);
		} catch (DALException e1) {
			System.out.println(e1);
			e1.printStackTrace();
		}	

    	UtilisateurManager manager = UtilisateurManager.getUtilisateurManager();
        try {
        	manager.updateUtilisateur(request, utilisateur);
		} catch (BusinessException e) {
			e.printStackTrace();
			request.setAttribute("listeCodesErreur", e.getListeCodesErreur());
			request.setAttribute( ATT_USER_INFOS, manager.getInfos((int) request.getSession().getAttribute(EncheresProperties.getProperty("ATT_USER"))) );
			this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );		
			return;
		} catch (Exception e) {
			e.printStackTrace();
		}
        
        response.sendRedirect(request.getContextPath() + "/visuProfile?pseudo=" + utilisateur.getPseudo());        
	}

}
