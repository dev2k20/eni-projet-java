package fr.eni.encheres.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import fr.eni.encheres.BusinessException;
import fr.eni.encheres.EncheresProperties;
import fr.eni.encheres.bll.UtilisateurManager;

/**
 * Servlet implementation class Inscription
 */
@WebServlet("/inscription")
public class Inscription extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public static final String VUE              = "/WEB-INF/inscription.jsp";
    public static final String VUE_ACCUEIL = "/accueil";

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* Affichage de la page de d'inscription ou d'accueil si l'utilisateur est connecté ou non */
    	if (request.getSession().getAttribute(EncheresProperties.getProperty("ATT_USER")) != null)
        	this.getServletContext().getRequestDispatcher( VUE_ACCUEIL ).forward( request, response );	
    	else 
    		this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* Préparation de l'objet formulaire */
        UtilisateurManager form = UtilisateurManager.getUtilisateurManager();
        

        try {
			form.inscription(request);
			//this.getServletContext().getRequestDispatcher(Connexion.VUE).forward(request, response);
		} catch (BusinessException e) {
			request.setAttribute("listeCodesErreur", e.getListeCodesErreur());
			this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
			return;
		} catch (Exception e) {
			e.printStackTrace();
			this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
			return;
		}
        
        
        ServletContext context = this.getServletContext();
        RequestDispatcher dispatcher = context.getRequestDispatcher("/connexion");
        dispatcher.forward(request, response);
        
	}

}
