package fr.eni.encheres.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eni.encheres.bo.Utilisateur;
import fr.eni.encheres.BusinessException;
import fr.eni.encheres.EncheresProperties;
import fr.eni.encheres.bll.UtilisateurManager;

@WebServlet("/connexion")
public class Connexion extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public static final String VUE              = "/WEB-INF/connexion.jsp";

    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        /* Affichage de la page de connexion ou d'accueil si l'utilisateur est connecté ou non */
    	if (request.getSession().getAttribute(EncheresProperties.getProperty("ATT_USER")) != null)
        	this.getServletContext().getRequestDispatcher( EncheresProperties.getProperty("VUE_ACCUEIL") ).forward( request, response );	
    	else 
    		this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
    }

    public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        /* Préparation de l'objet formulaire */
        UtilisateurManager form = UtilisateurManager.getUtilisateurManager();

        /* Récupération de la session depuis la requête */
        HttpSession session = request.getSession();
        
        /* Traitement de la requête et récupération du bean en résultant */
        Utilisateur utilisateur = null;
        String pageSuivante = "";
		try {
			utilisateur = form.connexion( request );
			
			/**
	         * Si aucune erreur de validation n'a eu lieu, alors ajout du bean
	         * Utilisateur à la session, sinon suppression du bean de la session.
	         */
			if(utilisateur != null) {
				session.setAttribute( EncheresProperties.getProperty("ATT_SESSION_USER"), utilisateur.getPseudo() );
				session.setAttribute( EncheresProperties.getProperty("ATT_USER"), utilisateur.getNo_utilisateur() );
				session.setAttribute( EncheresProperties.getProperty("ATT_CREDITS_USER"), utilisateur.getCredit());
				pageSuivante = EncheresProperties.getProperty("VUE_ACCUEIL");
				//response.sendRedirect( request.getContextPath() +  pageSuivante);
				this.getServletContext().getRequestDispatcher( pageSuivante ).forward( request, response );	
			} else {
				throw new BusinessException();	
			}
		} catch (BusinessException e) {
			pageSuivante = VUE;
			session.setAttribute( EncheresProperties.getProperty("ATT_SESSION_USER"), null );
			session.setAttribute( EncheresProperties.getProperty("ATT_USER"), null );
			request.setAttribute("listeCodesErreur", e.getListeCodesErreur());
		} catch (Exception e) {
			// TODO: handle exception
			pageSuivante = VUE;
		}
		//response.sendRedirect(  pageSuivante);
		if(utilisateur == null)
			this.getServletContext().getRequestDispatcher( pageSuivante ).forward( request, response );	

    }
}
