package fr.eni.encheres.servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sun.javafx.collections.MappingChange.Map;

import fr.eni.encheres.BusinessException;
import fr.eni.encheres.EncheresProperties;
import fr.eni.encheres.bll.CodesResultatBLL;
import fr.eni.encheres.bll.UtilisateurManager;
import fr.eni.encheres.bo.Utilisateur;
import fr.eni.encheres.dal.DALException;
import fr.eni.encheres.dal.DAOFactory;
import fr.eni.encheres.dal.UtilisateurDAO;

/**
 * Servlet implementation class VisuProfile
 */
@WebServlet("/visuProfile")
public class VisuProfile extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public static final String ATT_USER_INFOS   = "userInfos";
    public static final String VUE              = "/WEB-INF/visuProfile.jsp";
       
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub			
		if (request.getSession().getAttribute(EncheresProperties.getProperty("ATT_USER")) == null) {
			response.sendRedirect(request.getContextPath() + "/accueil");
		} else {
			UtilisateurManager manager = UtilisateurManager.getUtilisateurManager();
			UtilisateurDAO utilisateurDAO = DAOFactory.getUtilisateurDAO();
			Utilisateur utilisateur = new Utilisateur();
			try {
				utilisateur = utilisateurDAO.selectByPseudo((String) request.getParameter("pseudo"));
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			request.setAttribute( ATT_USER_INFOS, manager.getInfos(utilisateur.getNo_utilisateur()) );
			this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );	
		}		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
	}

}
