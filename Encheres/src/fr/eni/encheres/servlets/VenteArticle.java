package fr.eni.encheres.servlets;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eni.encheres.BusinessException;
import fr.eni.encheres.EncheresProperties;
import fr.eni.encheres.bll.ArticleManager;
import fr.eni.encheres.bll.CategorieManager;
import fr.eni.encheres.bll.UtilisateurManager;
import fr.eni.encheres.bo.Utilisateur;
import fr.eni.encheres.dal.DALException;

/**
 * Servlet implementation class VenteArticle
 */
@MultipartConfig
@WebServlet("/venteArticle")
public class VenteArticle extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String VUE = "/WEB-INF/venteArticle.jsp";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public VenteArticle() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        /* Affichage de la page de création de vente ou d'accueil si l'utilisateur est connecté ou non */
    	Object id = request.getSession().getAttribute(EncheresProperties.getProperty("ATT_USER"));
    	if (id != null)
    	{
    		CategorieManager categorieManager = CategorieManager.getCategorieManager();
    		UtilisateurManager utilisateurManager = UtilisateurManager.getUtilisateurManager();
    		try {
    			Map<String, String> utilisateur = utilisateurManager.getInfos((int) id);
    			request.setAttribute("rue", utilisateur.get("rue"));
    			request.setAttribute("ville", utilisateur.get("ville"));
    			request.setAttribute("code_postal", utilisateur.get("code_postal"));
				Map<Integer, String> categories = categorieManager.selectionnerTout(request);
				request.setAttribute("listeCategories", categories);
			} catch (BusinessException e) {
				request.setAttribute("listeCodesErreur", e.getListeCodesErreur());
				this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
				return;
			}
    		
        	this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );    
        	
    	}
    	else 
    		response.sendRedirect( request.getContextPath() +  EncheresProperties.getProperty("VUE_ACCUEIL"));
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArticleManager form = ArticleManager.getArticleManager();
		
		HttpSession session = request.getSession();
		
		request.setAttribute("no_utilisateur_vendeur", session.getAttribute(EncheresProperties.getProperty("ATT_USER")));
		
		try {
			form.ajoutArticle(request);
			//this.getServletContext().getRequestDispatcher(Connexion.VUE).forward(request, response);
			response.sendRedirect( request.getContextPath() +  EncheresProperties.getProperty("VUE_ACCUEIL"));
		} catch (BusinessException e) {
			request.setAttribute("listeCodesErreur", e.getListeCodesErreur());
			//response.sendRedirect( request.getContextPath() +  EncheresProperties.getProperty("VUE"));
			this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );	
		} catch (Exception e) {
			System.out.println(e);
			//response.sendRedirect( request.getContextPath() +  EncheresProperties.getProperty("VUE"));
			this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );	
		}
		
	}

}
