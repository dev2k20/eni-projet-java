package fr.eni.encheres.ihm;

import java.sql.Date;
import java.util.List;

import fr.eni.encheres.bo.ArticleVendu;
import fr.eni.encheres.bo.Categorie;
import fr.eni.encheres.bo.Enchere;
import fr.eni.encheres.bo.Retrait;
import fr.eni.encheres.bo.Utilisateur;
import fr.eni.encheres.dal.ArticleDAO;
import fr.eni.encheres.dal.CategorieDAO;
import fr.eni.encheres.dal.DALException;
import fr.eni.encheres.dal.DAOFactory;
import fr.eni.encheres.dal.EnchereDAO;
import fr.eni.encheres.dal.RetraitDAO;
import fr.eni.encheres.dal.UtilisateurDAO;
import fr.eni.encheres.dal.jdbc.InterfaceDAO;

class TestAppErwan {
    public static void main(String[] args) throws DALException {
        System.out.println("test" + getVersion());
        
        
        /////////////////// UTILISATEUR : TEST OK        
        UtilisateurDAO UtilisateurDAO = DAOFactory.getUtilisateurDAO();               
        Utilisateur user1 = new Utilisateur();
        user1.setPseudo("jp");
        user1.setNom("pierre");
        user1.setPrenom("jean");
        user1.setEmail("jp@gmail.com");
        user1.setTelephone("0102030405");
        user1.setRue("rue de la paix");
        user1.setVille("Paris");
        user1.setCode_postal("75000");
        user1.setMot_de_passe("MDP");
        user1.setCredit(10);
        user1.setAdministrateur(false);
                
        UtilisateurDAO.insert(user1);        
        System.out.println(UtilisateurDAO.selectAll());        
        
        Utilisateur user = UtilisateurDAO.selectById(user1.getNo_utilisateur());
                
        user.setPrenom("JeanBono");
        UtilisateurDAO.update(user);

        System.out.println(UtilisateurDAO.selectAll());
        

        
        
        
        
        
        
        
        ////////////////////////// CATEGORIE : TEST OK        
        CategorieDAO categorieDao = DAOFactory.getCategorieDAO();
        
        Categorie categorie1 = new Categorie();
        categorie1.setLibelle("Libellé 1");
        
        Categorie categorie2 = new Categorie();
        categorie2.setLibelle("CATEGORIE NUM 2");
        
        categorieDao.insert(categorie1);
        categorieDao.insert(categorie2);
        
        categorie1 = categorieDao.selectById(categorie1.getNoCategorie());
        
        categorie1.setLibelle("Libellé 1 edit");
        categorieDao.update(categorie1);
        List<Categorie> list = categorieDao.selectAll();
        System.out.println(list);
        
        
        
        
        ////////////////////// RETRAIT : TEST OK        
        RetraitDAO retraitDAO = DAOFactory.getRetraitDAO();
        Retrait retrait = new Retrait(-1, "rue mouftar", "75000", "Paris");
        
        retraitDAO.insert(retrait);        
        retrait = retraitDAO.selectById(retrait.getNoRetrait());
        retrait.setVille("Lyon");
        retraitDAO.update(retrait);
        System.out.println(retraitDAO.selectAll());
        
        
        
        
        //////////////////// ARTICLE : TEST OK
        ArticleDAO articleDAO = DAOFactory.getArticleDAO();
        ArticleVendu articleVendu = new ArticleVendu();
        articleVendu.setNomArticle("Article 1");
        articleVendu.setDescription("la description");
        articleVendu.setDateDebutEncheres(new Date(0));
        articleVendu.setDateFinEncheres(new Date(0));
        articleVendu.setMiseAPrix(100);
        articleVendu.setPrixVente(1000);
        articleVendu.setEtatVente(5);
        articleVendu.setAcheteur(user1);
        articleVendu.setVendeur(user1);
        articleVendu.setCategorie(categorie1);
        articleVendu.setAdresse(retrait);

        articleDAO.insert(articleVendu);        
        articleVendu = articleDAO.selectById(articleVendu.getNoArticle());
        articleVendu.setNomArticle("EDITED");
        articleDAO.update(articleVendu);        
        System.out.println(articleDAO.selectAll());

        
        
        
        ///////////////// ENCHERE
        EnchereDAO enchereDAO = DAOFactory.getEnchereDAO();
        Enchere enchere = new Enchere();
        enchere.setArticle(articleVendu);
        enchere.setDateEnchere(new Date(0));
        enchere.setEncherisseur(user);
        enchere.setMontantEnchere(9999);
        
        enchereDAO.insert(enchere);
        System.out.println(enchereDAO.selectAll());
        
        
        // DELETE
        
        enchereDAO.deleteAll();;
        
        articleDAO.delete(articleVendu.getNoArticle());
        
        retraitDAO.delete(retrait.getNoRetrait());
        
        categorieDao.delete(categorie1.getNoCategorie());
        categorieDao.delete(categorie2.getNoCategorie());    

        UtilisateurDAO.delete(user1.getNo_utilisateur());
    }
    
    private static int getVersion() {
        String version = System.getProperty("java.version");
        if(version.startsWith("1.")) {
            version = version.substring(2, 3);
        } else {
            int dot = version.indexOf(".");
            if(dot != -1) { version = version.substring(0, dot); }
        } return Integer.parseInt(version);
    }
}