package fr.eni.encheres;

import java.io.IOException;
import java.util.Properties;


public class EncheresProperties {
    private static Properties properties;
    private static String langue;
    /**
     * Bloc static s'exécute dès le chargement de la classe
     */
    static {
        properties = new Properties();
        	
    	langue = System.getProperty("user.language");
        
    	try 
    	{
    		/**
             * class.getResourceAsStream -- permet de déterminer le répertoire de la classe
             */
    		if (langue.toLowerCase().equals("en"))
    		{
    			properties.load(EncheresProperties.class.getResourceAsStream("texteEN.properties"));
    			langue = "en";
    		} else 
    		{
    			properties.load(EncheresProperties.class.getResourceAsStream("texteFR.properties"));
    			langue = "fr";
    		}
        			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    }
    /**
     *
     * @param key
     * @return la propriété associée à la clef
     */
    public static String getProperty(String key) {
        return properties.getProperty(key, null);
    }
    
    public static void changerLangue(String langueSaisie) {
    	try 
    	{
    		/**
             * class.getResourceAsStream -- permet de déterminer le répertoire de la classe
             */
    		if (langueSaisie.toLowerCase().equals("en"))
    		{
    			properties.load(EncheresProperties.class.getResourceAsStream("texteFR.properties"));
    			langue = "fr";
    		} else 
    		{
    			properties.load(EncheresProperties.class.getResourceAsStream("texteEN.properties"));
    			langue = "en";
    		}
        			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public static String getLangue() {
    	return langue;
    }

}