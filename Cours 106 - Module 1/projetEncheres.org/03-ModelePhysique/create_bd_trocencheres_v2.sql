-- Script de création de la base de données ENCHERES
--   type :      SQL Server 2012
--
DROP TABLE IF EXISTS RETRAITS;
DROP TABLE IF EXISTS ENCHERES;
DROP TABLE IF EXISTS ARTICLES_VENDUS;
DROP TABLE IF EXISTS CATEGORIES;
DROP TABLE IF EXISTS UTILISATEURS;
DROP TABLE IF EXISTS ETAT;




CREATE TABLE CATEGORIES (
    no_categorie   INTEGER IDENTITY(1,1) NOT NULL,
    libelle        VARCHAR(30) NOT NULL
)

ALTER TABLE CATEGORIES ADD constraint categorie_pk PRIMARY KEY (no_categorie)

CREATE TABLE ENCHERES (
    no_utilisateur   INTEGER NOT NULL,
    no_article       INTEGER NOT NULL,
    date_enchere     datetime NOT NULL,
	montant_enchere  INTEGER NOT NULL

)

ALTER TABLE ENCHERES ADD constraint enchere_pk PRIMARY KEY (no_utilisateur, no_article)

CREATE TABLE RETRAITS (
	no_retrait       INTEGER IDENTITY(1,1) NOT NULL,
    rue              VARCHAR(255) NOT NULL, -- noms de rue peuvent être > 30 charactères
    code_postal      VARCHAR(10) NOT NULL,
    ville            VARCHAR(255) NOT NULL
)

ALTER TABLE RETRAITS ADD constraint retrait_pk PRIMARY KEY  (no_retrait)

-- Changement de la taille du mot_de_passe car on le stock pas en clair
CREATE TABLE UTILISATEURS (
    no_utilisateur   INTEGER IDENTITY(1,1) NOT NULL,
    pseudo           VARCHAR(30) NOT NULL,
    nom              VARCHAR(255) NOT NULL,
    prenom           VARCHAR(255) NOT NULL,
    email            VARCHAR(255) NOT NULL,
    telephone        VARCHAR(15),
    rue              VARCHAR(255) NOT NULL,
    code_postal      VARCHAR(10) NOT NULL,
    ville            VARCHAR(255) NOT NULL,
    mot_de_passe     VARCHAR(255) NOT NULL,
    credit           INTEGER NOT NULL,
    administrateur   bit NOT NULL
)

ALTER TABLE UTILISATEURS ADD constraint utilisateur_pk PRIMARY KEY (no_utilisateur)


CREATE TABLE ARTICLES_VENDUS (
    no_article                    INTEGER IDENTITY(1,1) NOT NULL,
    nom_article                   VARCHAR(30) NOT NULL,
    description                   VARCHAR(300) NOT NULL,
	date_debut_encheres           DATETIME NOT NULL, -- Changement à DATETIME pour heure/minute
    date_fin_encheres             DATETIME NOT NULL,
    prix_initial                  INTEGER,
    prix_vente                    INTEGER,
	etat_vente                    INTEGER NOT NULL DEFAULT(0),
    no_utilisateur_vendeur     	  INTEGER NOT NULL, -- ajout vendeur au lieu de no_utilisateur
	no_utilisateur_acheteur    	  INTEGER NULL, -- ajout acheteur
    no_categorie                  INTEGER NOT NULL,
	no_retrait					  INTEGER NOT NULL, -- ajout 
	image						  VARBINARY(MAX) NULL 
)

ALTER TABLE ARTICLES_VENDUS ADD constraint articles_vendus_pk PRIMARY KEY (no_article)

CREATE TABLE ETAT (
    no_etat   INTEGER IDENTITY(1,1) NOT NULL,
    libelle        VARCHAR(30) NOT NULL
)

ALTER TABLE ETAT ADD constraint etat_pk PRIMARY KEY (no_etat)

ALTER TABLE ENCHERES
    ADD CONSTRAINT encheres_articles_vendus_fk FOREIGN KEY ( no_article )
        REFERENCES ARTICLES_VENDUS ( no_article )
ON DELETE NO ACTION 
    ON UPDATE no action 

ALTER TABLE ARTICLES_VENDUS
    ADD CONSTRAINT articles_vendus_categories_fk FOREIGN KEY ( no_categorie )
        REFERENCES categories ( no_categorie )
ON DELETE NO ACTION 
    ON UPDATE no action 

ALTER TABLE ARTICLES_VENDUS -- FK VENDEUR
    ADD CONSTRAINT ventes_utilisateur_fk FOREIGN KEY ( no_utilisateur_vendeur )
        REFERENCES utilisateurs ( no_utilisateur )
ON DELETE NO ACTION 
    ON UPDATE no action 
	
	ALTER TABLE ARTICLES_VENDUS -- FK ACHETEUR
    ADD CONSTRAINT acheteur_utilisateur_fk FOREIGN KEY ( no_utilisateur_acheteur )
        REFERENCES utilisateurs ( no_utilisateur )
ON DELETE NO ACTION 
    ON UPDATE no action 
	
ALTER TABLE ARTICLES_VENDUS -- FK retrait
    ADD CONSTRAINT article_retrait_fk FOREIGN KEY ( no_retrait )
        REFERENCES retraits ( no_retrait )
ON DELETE NO ACTION 
    ON UPDATE no action 
	
ALTER TABLE ARTICLES_VENDUS -- FK etat
    ADD CONSTRAINT article_etat_fk FOREIGN KEY ( etat_vente )
        REFERENCES etat ( no_etat )
ON DELETE NO ACTION 
    ON UPDATE no action 

USE [Encheres]
GO
SET IDENTITY_INSERT [dbo].[CATEGORIES] ON 

INSERT [dbo].[CATEGORIES] ([no_categorie], [libelle]) VALUES (1, N'Ameublement')
INSERT [dbo].[CATEGORIES] ([no_categorie], [libelle]) VALUES (2, N'Informatique')
INSERT [dbo].[CATEGORIES] ([no_categorie], [libelle]) VALUES (3, N'Sport et Loisirs')
INSERT [dbo].[CATEGORIES] ([no_categorie], [libelle]) VALUES (4, N'Vêtement')
SET IDENTITY_INSERT [dbo].[CATEGORIES] OFF
GO
SET IDENTITY_INSERT [dbo].[RETRAITS] ON 

INSERT [dbo].[RETRAITS] ([no_retrait], [rue], [code_postal], [ville]) VALUES (1, N'32 rue Banaudon', N'35000', N'Rennes')
INSERT [dbo].[RETRAITS] ([no_retrait], [rue], [code_postal], [ville]) VALUES (2, N'7 rue Lenotre', N'35700', N'Rennes')
SET IDENTITY_INSERT [dbo].[RETRAITS] OFF
GO
SET IDENTITY_INSERT [dbo].[UTILISATEURS] ON 

INSERT [dbo].[UTILISATEURS] ([no_utilisateur], [pseudo], [nom], [prenom], [email], [telephone], [rue], [code_postal], [ville], [mot_de_passe], [credit], [administrateur]) VALUES (1, N'admin', N'admin', N'admin', N'admin@gmail.com', N'0606060606', N'10 rue de la Poste', N'35700', N'Rennes', N'8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 0, 0)
INSERT [dbo].[UTILISATEURS] ([no_utilisateur], [pseudo], [nom], [prenom], [email], [telephone], [rue], [code_postal], [ville], [mot_de_passe], [credit], [administrateur]) VALUES (2, N'utilisateur', N'Bernard', N'Jean', N'utilisateur@gmail.com', N'0606060606', N'7 rue Lenotre', N'35700', N'Rennes', N'dd10ddc914f2528f71534cfbf6d73a9fadd3661efb09ae6d855c2d73fa81cc6b', 0, 0)
INSERT [dbo].[UTILISATEURS] ([no_utilisateur], [pseudo], [nom], [prenom], [email], [telephone], [rue], [code_postal], [ville], [mot_de_passe], [credit], [administrateur]) VALUES (3, N'vendeur', N'Ebay', N'Amazon', N'vendeur@gmail.com', N'0606060606', N'32 rue Banaudon', N'35000', N'Rennes', N'3fe6dd5dd172cef095720595fe45bac03bdfd9844d68a5b7dfc20320437aba92', 0, 0)
INSERT [dbo].[UTILISATEURS] ([no_utilisateur], [pseudo], [nom], [prenom], [email], [telephone], [rue], [code_postal], [ville], [mot_de_passe], [credit], [administrateur]) VALUES (4, N'utilisateur2', N'Gabriaux', N'Luc', N'utilisateur2@gmail.com', N'0606060606', N'80 rue Cazade', N'35450', N'Rennes', N'a2a7f6d242587f4789cc92ca7292bc734395d86a312245afa0f9ab8552cd1afb', 0, 0)
SET IDENTITY_INSERT [dbo].[UTILISATEURS] OFF
GO
SET IDENTITY_INSERT [dbo].[ETAT] ON 

INSERT [dbo].[ETAT] ([no_etat], [libelle]) VALUES (1, N'Créée')
INSERT [dbo].[ETAT] ([no_etat], [libelle]) VALUES (2, N'En cours')
INSERT [dbo].[ETAT] ([no_etat], [libelle]) VALUES (3, N'Enchères terminées')
INSERT [dbo].[ETAT] ([no_etat], [libelle]) VALUES (4, N'Retrait effectué')
SET IDENTITY_INSERT [dbo].[ETAT] OFF
GO
SET IDENTITY_INSERT [dbo].[ARTICLES_VENDUS] ON 

INSERT [dbo].[ARTICLES_VENDUS] ([no_article], [nom_article], [description], [date_debut_encheres], [date_fin_encheres], [prix_initial], [prix_vente], [etat_vente], [no_utilisateur_vendeur], [no_utilisateur_acheteur], [no_categorie], [no_retrait], [image]) VALUES (1, N'Télévision 4k 55"', N'Télévision en bon état d''une taille de 55 pouces', CAST(N'2020-09-20T14:00:00.000' AS DateTime), CAST(N'2020-09-25T14:00:00.000' AS DateTime), 200, NULL, 1, 3, NULL, 2, 1, 0x)
INSERT [dbo].[ARTICLES_VENDUS] ([no_article], [nom_article], [description], [date_debut_encheres], [date_fin_encheres], [prix_initial], [prix_vente], [etat_vente], [no_utilisateur_vendeur], [no_utilisateur_acheteur], [no_categorie], [no_retrait], [image]) VALUES (2, N'Manteau d''hiver', N'Manteau en fourrure en parfait état pour passer un hiver au chaud', CAST(N'2020-09-16T12:00:00.000' AS DateTime), CAST(N'2020-09-30T18:00:00.000' AS DateTime), 100, NULL, 1, 3, NULL, 4, 1, 0x)
INSERT [dbo].[ARTICLES_VENDUS] ([no_article], [nom_article], [description], [date_debut_encheres], [date_fin_encheres], [prix_initial], [prix_vente], [etat_vente], [no_utilisateur_vendeur], [no_utilisateur_acheteur], [no_categorie], [no_retrait], [image]) VALUES (3, N'VTT', N'Vélo tout terrain ', CAST(N'2020-09-25T12:31:00.000' AS DateTime), CAST(N'2020-09-25T12:32:00.000' AS DateTime), 120, NULL, 1, 3, NULL, 3, 1, 0x)
INSERT [dbo].[ARTICLES_VENDUS] ([no_article], [nom_article], [description], [date_debut_encheres], [date_fin_encheres], [prix_initial], [prix_vente], [etat_vente], [no_utilisateur_vendeur], [no_utilisateur_acheteur], [no_categorie], [no_retrait], [image]) VALUES (4, N'Table basse', N'Table basse IKEA comme neuve', CAST(N'2020-09-20T11:24:00.000' AS DateTime), CAST(N'2020-09-22T11:24:00.000' AS DateTime), 20, NULL, 1, 3, NULL, 1, 1, 0x)
INSERT [dbo].[ARTICLES_VENDUS] ([no_article], [nom_article], [description], [date_debut_encheres], [date_fin_encheres], [prix_initial], [prix_vente], [etat_vente], [no_utilisateur_vendeur], [no_utilisateur_acheteur], [no_categorie], [no_retrait], [image]) VALUES (5, N'Chemise', N'Chemise neuve', CAST(N'2020-10-08T08:00:00.000' AS DateTime), CAST(N'2020-10-14T20:00:00.000' AS DateTime), 15, NULL, 1, 2, NULL, 4, 2, 0x)
SET IDENTITY_INSERT [dbo].[ARTICLES_VENDUS] OFF
GO
INSERT [dbo].[ENCHERES] ([no_utilisateur], [no_article], [date_enchere], [montant_enchere]) VALUES (1, 5, CAST(N'2020-09-18T00:00:00.000' AS DateTime), 25)
INSERT [dbo].[ENCHERES] ([no_utilisateur], [no_article], [date_enchere], [montant_enchere]) VALUES (2, 1, CAST(N'2020-09-18T00:00:00.000' AS DateTime), 350)
INSERT [dbo].[ENCHERES] ([no_utilisateur], [no_article], [date_enchere], [montant_enchere]) VALUES (4, 1, CAST(N'2020-09-18T00:00:00.000' AS DateTime), 220)
GO

