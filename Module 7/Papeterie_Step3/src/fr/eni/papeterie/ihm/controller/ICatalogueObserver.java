package fr.eni.papeterie.ihm.controller;

public interface ICatalogueObserver {
	void miseAJourDesDonnees();
}
