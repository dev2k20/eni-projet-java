package fr.eni.papeterie.ihm.view;

import java.awt.GridLayout;
import java.awt.Toolkit;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import fr.eni.papeterie.bo.Article;

@SuppressWarnings("serial")
public class EcranCatalogue extends JFrame {
	private TableCatalogue tblCatalogue;

	public EcranCatalogue() {
		super("Catalogue");
		setSize(600, 300);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		Toolkit tk = Toolkit.getDefaultToolkit();
		setIconImage(tk.getImage(getClass().getResource("../resources/aim.png")));
	}

	public void initComposants(List<Article> articles) {
		JPanel panel = new JPanel();
		panel.setOpaque(true);
		panel.setLayout(new GridLayout(1, 0));
		tblCatalogue = new TableCatalogue(articles);
		JScrollPane scrollPane = new JScrollPane(tblCatalogue);
		panel.add(scrollPane);
		setContentPane(panel);
		setVisible(true);
	}
	

	public void popup(String title, String msg, int logo) {
		JOptionPane.showMessageDialog(EcranCatalogue.this, msg, title, logo);
	}
	
	public TableCatalogue getTblCatalogue() {
		return tblCatalogue;
	}
}
