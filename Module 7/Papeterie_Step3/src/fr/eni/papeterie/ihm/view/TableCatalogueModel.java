package fr.eni.papeterie.ihm.view;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import fr.eni.papeterie.bo.Article;
import fr.eni.papeterie.bo.Stylo;

@SuppressWarnings("serial")
public class TableCatalogueModel extends AbstractTableModel {
	private List<Article> catalogue;
	private String[] columnNames = { "", "Reference", "Marque", "Designation", "Prix Unitaire", "Stock" };

	public TableCatalogueModel(List<Article> catalogue) {
		super();
		this.catalogue = catalogue;
	}

	@Override
	public String getColumnName(int column) {
		return columnNames[column];
	}

	@Override
	public int getRowCount() {
		return catalogue.size();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Object val = null;
		Article art = catalogue.get(rowIndex);
		switch (columnIndex) {
		case TableCatalogue.COL_ICON:
			// Permet de déterminer si nous avons un Stylo ou une ramette pour le logo
			val = art instanceof Stylo ? "S" : "R";
			break;
		case TableCatalogue.COL_REFERENCE:
			val = art.getReference();
			break;
		case TableCatalogue.COL_MARQUE:
			val = art.getMarque();
			break;
		case TableCatalogue.COL_DESIGNATION:
			val = art.getDesignation();
			break;
		case TableCatalogue.COL_PU:
			val = art.getPrixUnitaire();
			break;
		default:
			// STOCK
			val = art.getQteStock();
			break;
		}
		return val;
	}

}
