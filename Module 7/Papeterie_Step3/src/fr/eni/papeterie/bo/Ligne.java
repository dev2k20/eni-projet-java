package fr.eni.papeterie.bo;

public class Ligne {
	//Association
	private Article article;
	protected int qte;
	
	public Ligne() {
	}

	public Ligne(Article article, int qte) {
		this.article = article;
		this.qte = qte;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public int getQte() {
		return qte;
	}

	public void setQte(int qte) {
		this.qte = qte;
	}

	/**
	 * Permet de gérer l'encapsulation
	 */
	public float getPrix() {
		return article.getPrixUnitaire();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((article == null) ? 0 : article.hashCode());
		result = prime * result + qte;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ligne other = (Ligne) obj;
		if (article == null) {
			if (other.article != null)
				return false;
		} else if (!article.equals(other.article))
			return false;
		if (qte != other.qte)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return String.format("Ligne [%s, qte=%s]", article, qte);
	}
	
	
}
