package fr.eni.papeterie.dal.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import fr.eni.papeterie.bo.Article;
import fr.eni.papeterie.bo.Ramette;
import fr.eni.papeterie.bo.Stylo;
import fr.eni.papeterie.dal.ArticleDAO;
import fr.eni.papeterie.dal.DALException;

/**
 * @author Eni Ecole
 * 
 */
public class ArticleDAOJdbcImpl implements ArticleDAO {
	private static final String INSERT = "insert into articles (reference,marque,designation,prixUnitaire, qteStock, grammage, couleur, type) values (?,?,?,?,?,?,?,?);";
	private static final String SELECT_BY_ID = "select idArticle, reference,marque,designation,prixUnitaire, qteStock, grammage, couleur, type from articles where idArticle = ?";
	private static final String SELECT_ALL = "select idArticle, reference,marque,designation,prixUnitaire, qteStock, grammage, couleur, type from articles";
	private static final String UPDATE = "update articles set reference = ?, marque = ?, designation =  ?, prixUnitaire = ?, qteStock = ?, grammage = ?, couleur = ? where idArticle = ?;";
	private static final String DELETE = "delete articles where idArticle = ?;";
	private static final String SELECT_BY_MARQUE = "select idArticle, reference,marque,designation,prixUnitaire, qteStock, grammage, couleur, type from articles where marque = ?";
	private static final String SELECT_BY_MOT_CLEF = "select idArticle, reference,marque,designation,prixUnitaire, qteStock, grammage, couleur, type from articles where marque like ? or designation like ?";

	private static final String TYPE_RAMETTE = Ramette.class.getSimpleName();
	private static final String TYPE_STYLO = Stylo.class.getSimpleName();

	public void insert(Article art) throws DALException {
		try (Connection con = JdbcTools.getConnection();
				PreparedStatement stm = con.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);) {
			// injecter les paramètres
			setParameter(stm, art);
			if (art instanceof Stylo) {
				stm.setString(8, TYPE_STYLO);
			} else if (art instanceof Ramette) {
				stm.setString(8, TYPE_RAMETTE);
			}

			int nbEnr = stm.executeUpdate();
			if (nbEnr == 1) {
				// Pour récupérer l'identity généré
				ResultSet rs = stm.getGeneratedKeys();
				if (rs.next()) {
					int id = rs.getInt(1);
					art.setIdArticle(id);// Mise à jour de l'attribut id
				}
			} else {
				throw new DALException("Impossible d'enregistrer - " + art);
			}

		} catch (SQLException e) {
			throw new DALException("Erreur Technique - " + e.getMessage());
		}
	}

	public Article selectById(int idArticle) throws DALException {
		try (Connection con = JdbcTools.getConnection(); PreparedStatement stm = con.prepareStatement(SELECT_BY_ID);) {
			stm.setInt(1, idArticle);
			ResultSet rs = stm.executeQuery();
			Article art = null;
			if (rs.next()) {
				art = mapping(rs);
				return art;
			} else {
				throw new DALException("L'article n'existe pas - id = " + idArticle);
			}

		} catch (SQLException e) {
			throw new DALException("Erreur Technique - " + e.getMessage());
		}
	}

	public List<Article> selectAll() throws DALException {
		List<Article> liste = new ArrayList<>();
		try (Connection con = JdbcTools.getConnection(); Statement stm = con.createStatement();) {
			ResultSet rs = stm.executeQuery(SELECT_ALL);
			Article art = null;
			while (rs.next()) {
				art = mapping(rs);
				liste.add(art);
			}
			return liste;

		} catch (SQLException e) {
			throw new DALException("Erreur Technique - " + e.getMessage());
		}
	}

	public void update(Article art) throws DALException {
		try (Connection con = JdbcTools.getConnection(); PreparedStatement stm = con.prepareStatement(UPDATE);) {
			setParameter(stm, art);
			stm.setInt(8, art.getIdArticle());
			int nbEnr = stm.executeUpdate();
			if (nbEnr != 1) {
				throw new DALException("Impossible de mettre à jour - " + art);
			}

		} catch (SQLException e) {
			throw new DALException("Erreur Technique - " + e.getMessage());
		}

	}

	public void delete(int idArticle) throws DALException {
		try (Connection con = JdbcTools.getConnection(); PreparedStatement stm = con.prepareStatement(DELETE);) {
			stm.setInt(1, idArticle);
			int nbEnr = stm.executeUpdate();
			if (nbEnr != 1) {
				throw new DALException("Impossible de supprimer - " + idArticle);
			}

		} catch (SQLException e) {
			throw new DALException("Erreur Technique - " + e.getMessage());
		}
	}

	private void setParameter(PreparedStatement stm, Article art) throws SQLException {
		// injecter les paramètres
		// grammage, couleur, type
		stm.setString(1, art.getReference());// 1 -> correspond au premier ? de la requête
		stm.setString(2, art.getMarque());
		stm.setString(3, art.getDesignation());
		stm.setFloat(4, art.getPrixUnitaire());
		stm.setInt(5, art.getQteStock());
		if (art instanceof Stylo) {
			stm.setNull(6, Types.INTEGER);
			Stylo s = (Stylo) art;// Transtypage -- Caster sur le type enfant
			stm.setString(7, s.getCouleur());

		} else if (art instanceof Ramette) {
			Ramette r = (Ramette) art;
			stm.setInt(6, r.getGrammage());
			stm.setNull(7, Types.NVARCHAR);
		}

	}

	private Article mapping(ResultSet rs) throws SQLException {
		Article art = null;
		int idArticle = rs.getInt("idArticle");
		String type = rs.getString("type").trim();
		String reference = rs.getString("reference").trim();
		String marque = rs.getString("marque").trim();
		String designation = rs.getString("designation").trim();
		float prixUnitaire = rs.getFloat("prixUnitaire");
		int qteStock = rs.getInt("qteStock");
		if (type.equalsIgnoreCase(TYPE_RAMETTE)) {
			int grammage = rs.getInt("grammage");
			art = new Ramette(idArticle, marque, reference, designation, prixUnitaire, qteStock, grammage);
		} else if (type.equalsIgnoreCase(TYPE_STYLO)) {
			String couleur = rs.getString("couleur");
			art = new Stylo(idArticle, marque, reference, designation, prixUnitaire, qteStock, couleur);
		}
		return art;
	}

	@Override
	public List<Article> selectByMarque(String marque) throws DALException {
		try (Connection con = JdbcTools.getConnection();
				PreparedStatement stm = con.prepareStatement(SELECT_BY_MARQUE);) {
			stm.setString(1, marque);
			ResultSet rs = stm.executeQuery();
			Article art = null;
			List<Article> liste = new ArrayList<>();
			while (rs.next()) {
				art = mapping(rs);
				liste.add(art);
			}
			return liste;
		} catch (SQLException e) {
			throw new DALException("Erreur Technique - " + e.getMessage());
		}
	}

	@Override
	public List<Article> selectByMotCle(String motCle) throws DALException {
		try (Connection con = JdbcTools.getConnection();
				PreparedStatement stm = con.prepareStatement(SELECT_BY_MOT_CLEF);) {
			String param = "%" + motCle + "%";
			stm.setString(1, param);
			stm.setString(2, param);
			ResultSet rs = stm.executeQuery();
			Article art = null;
			List<Article> liste = new ArrayList<>();
			while (rs.next()) {
				art = mapping(rs);
				liste.add(art);
			}
			return liste;
		} catch (SQLException e) {
			throw new DALException("Erreur Technique - " + e.getMessage());
		}
	}
}
