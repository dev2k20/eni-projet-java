package fr.eni.papeterie.ihm.view;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class EcranArticle extends JFrame {
	/* Formulaire de saisie */
	private JTextField txtReference, txtDesignation, txtMarque, txtStock, txtPrix;
	private JRadioButton radioRamette, radioStylo;
	private JPanel panelType, panelGrammage;
	private JCheckBox chk80, chk100;
	private JComboBox<String> cboCouleur;

	/* Boutons */
	private JPanel panelBoutons;
	private JButton btnPrecedent;
	private JButton btnNouveau;
	private JButton btnEnregistrer;
	private JButton btnSupprimer;
	private JButton btnSuivant;

	public EcranArticle() {
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setSize(500, 400);
		setResizable(false);
		setTitle("Détail article");
		ImageIcon icon = new ImageIcon(this.getClass().getResource("../resources/aim.png"));
		setIconImage(icon.getImage());
	}

	public void selectedStyloOrRamette(boolean isStylo, boolean isRamette) {
		getRadioStylo().setSelected(isStylo);
		getRadioRamette().setSelected(isRamette);
		getChk100().setEnabled(isRamette);
		getChk80().setEnabled(isRamette);
		getChk80().setSelected(isRamette);
		getCboCouleur().setEnabled(isStylo);
	}

	public void selectedCouleurGrammage(String couleur, int grammage) {
		// Mise à jour de la sélection de la couleur
		getCboCouleur().setSelectedItem(couleur);
		// Mise à jour de la valeur de ramette
		getChk80().setSelected(grammage == 80);
		getChk100().setSelected(grammage == 100);
	}

	public void initIhm() {
		JPanel panelPrincipal = new JPanel();
		panelPrincipal.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();

		gbc.insets = new Insets(5, 5, 5, 5);

		// Ligne 1
		gbc.gridy = 0;
		gbc.gridx = 0;
		panelPrincipal.add(new JLabel("Référence"), gbc);

		gbc.gridx = 1;
		panelPrincipal.add(getTxtReference(), gbc);

		// Ligne 2
		gbc.gridy = 1;
		gbc.gridx = 0;
		panelPrincipal.add(new JLabel("Désignation"), gbc);

		gbc.gridx = 1;
		panelPrincipal.add(getTxtDesignation(), gbc);

		// Ligne 3
		gbc.gridy = 2;
		gbc.gridx = 0;
		panelPrincipal.add(new JLabel("Marque"), gbc);

		gbc.gridx = 1;
		panelPrincipal.add(getTxtMarque(), gbc);

		// Ligne 4
		gbc.gridy = 3;
		gbc.gridx = 0;
		panelPrincipal.add(new JLabel("Stock"), gbc);

		gbc.gridx = 1;
		panelPrincipal.add(getTxtStock(), gbc);

		// Ligne 5
		gbc.gridy = 4;
		gbc.gridx = 0;
		panelPrincipal.add(new JLabel("Prix"), gbc);

		gbc.gridx = 1;
		panelPrincipal.add(getTxtPrix(), gbc);

		// Ligne 6
		gbc.gridy = 5;
		gbc.gridx = 0;
		panelPrincipal.add(new JLabel("Type"), gbc);

		gbc.gridx = 1;
		gbc.gridheight = 1;
		panelPrincipal.add(getPanelType(), gbc);

		// Ligne 7
		gbc.gridy = 6;
		gbc.gridx = 0;
		panelPrincipal.add(new JLabel("Grammage"), gbc);

		gbc.gridx = 1;
		panelPrincipal.add(getPanelGrammage(), gbc);

		// Ligne 8
		gbc.gridy = 7;

		gbc.gridx = 0;
		panelPrincipal.add(new JLabel("Couleur"), gbc);

		gbc.gridx = 1;
		panelPrincipal.add(getCboCouleur(), gbc);

		// Ligne 9
		gbc.gridy = 8;

		gbc.gridx = 0;
		gbc.gridwidth = 2;
		panelPrincipal.add(getPanelBoutons(), gbc);

		setContentPane(panelPrincipal);
		setVisible(true);

		// Initialise l'affichage des JRadioButton et JCkeckBox
		selectedStyloOrRamette(false, true);
		selectedCouleurGrammage(null, 80);
	}

	public JTextField getTxtReference() {
		if (txtReference == null) {
			txtReference = new JTextField(30);
		}
		return txtReference;
	}

	public JTextField getTxtDesignation() {
		if (txtDesignation == null) {
			txtDesignation = new JTextField(30);
		}
		return txtDesignation;
	}

	public JTextField getTxtMarque() {
		if (txtMarque == null) {
			txtMarque = new JTextField(30);
		}
		return txtMarque;
	}

	public JTextField getTxtStock() {
		if (txtStock == null) {
			txtStock = new JTextField(30);
		}
		return txtStock;
	}

	public JTextField getTxtPrix() {
		if (txtPrix == null) {
			txtPrix = new JTextField(30);
		}
		return txtPrix;
	}

	public JPanel getPanelType() {
		if (panelType == null) {
			panelType = new JPanel();
			panelType.setLayout(new BoxLayout(panelType, BoxLayout.Y_AXIS));
			panelType.add(getRadioRamette());
			panelType.add(getRadioStylo());
			ButtonGroup bg = new ButtonGroup();
			bg.add(getRadioRamette());
			bg.add(getRadioStylo());
		}
		return panelType;
	}

	public JPanel getPanelGrammage() {
		if (panelGrammage == null) {
			panelGrammage = new JPanel();
			panelGrammage.setLayout(new BoxLayout(panelGrammage, BoxLayout.Y_AXIS));
			panelGrammage.add(getChk80());
			panelGrammage.add(getChk100());
			ButtonGroup bg = new ButtonGroup();
			bg.add(getChk80());
			bg.add(getChk100());
		}

		return panelGrammage;
	}

	public JRadioButton getRadioRamette() {
		if (radioRamette == null) {
			radioRamette = new JRadioButton("Ramette");
		}
		return radioRamette;
	}

	public JRadioButton getRadioStylo() {
		if (radioStylo == null) {
			radioStylo = new JRadioButton("Stylo");
		}

		return radioStylo;
	}

	public JCheckBox getChk80() {
		if (chk80 == null) {
			chk80 = new JCheckBox("80 grammes");
		}
		return chk80;
	}

	public JCheckBox getChk100() {
		if (chk100 == null) {
			chk100 = new JCheckBox("100 grammes");
		}

		return chk100;
	}

	public JComboBox<String> getCboCouleur() {
		if (cboCouleur == null) {
			String[] couleurs = { "noir", "rouge", "vert", "bleu", "jaune" };
			cboCouleur = new JComboBox<String>(couleurs);
			// Placer sur une valeur vide
			cboCouleur.setSelectedItem(null);
		}
		return cboCouleur;
	}

	public JPanel getPanelBoutons() {
		if (panelBoutons == null) {
			panelBoutons = new JPanel();
			panelBoutons.setLayout(new FlowLayout());
			panelBoutons.add(getBtnPrecedent());
			panelBoutons.add(getBtnNouveau());
			panelBoutons.add(getBtnEnregistrer());
			panelBoutons.add(getBtnSupprimer());
			panelBoutons.add(getBtnSuivant());
		}

		return panelBoutons;
	}

	public JButton getBtnPrecedent() {
		if (btnPrecedent == null) {
			btnPrecedent = new JButton();
			ImageIcon image = new ImageIcon(this.getClass().getResource("../resources/Back24.gif"));
			btnPrecedent.setIcon(image);
		}
		return btnPrecedent;
	}

	public JButton getBtnNouveau() {
		if (btnNouveau == null) {
			btnNouveau = new JButton();
			ImageIcon image = new ImageIcon(this.getClass().getResource("../resources/New24.gif"));
			btnNouveau.setIcon(image);
		}

		return btnNouveau;
	}

	public JButton getBtnEnregistrer() {
		if (btnEnregistrer == null) {
			btnEnregistrer = new JButton();
			ImageIcon image = new ImageIcon(this.getClass().getResource("../resources/Save24.gif"));
			btnEnregistrer.setIcon(image);
		}
		return btnEnregistrer;
	}

	public JButton getBtnSupprimer() {
		if (btnSupprimer == null) {
			btnSupprimer = new JButton();
			ImageIcon image = new ImageIcon(this.getClass().getResource("../resources/Delete24.gif"));
			btnSupprimer.setIcon(image);
		}
		return btnSupprimer;
	}

	public JButton getBtnSuivant() {
		if (btnSuivant == null) {
			btnSuivant = new JButton();
			ImageIcon image = new ImageIcon(this.getClass().getResource("../resources/Forward24.gif"));
			btnSuivant.setIcon(image);
		}
		return btnSuivant;
	}

	public void popup(String title, String msg, int logo) {
		JOptionPane.showMessageDialog(EcranArticle.this, msg, title, logo);
	}

}