package fr.eni.papeterie.ihm.controller;

import javax.swing.JOptionPane;

import fr.eni.papeterie.bll.BLLException;
import fr.eni.papeterie.bll.CatalogueManager;
import fr.eni.papeterie.ihm.view.EcranCatalogue;

public class ControllerCatalogue {
	private EcranCatalogue ecran;
	private CatalogueManager mger;

	public ControllerCatalogue() {
		ecran = new EcranCatalogue();
	}

	public void showEcranCatalogue() {
		try {
			mger = CatalogueManager.getInstance();
			ecran.initComposants(mger.getCatalogue());
		} catch (BLLException e) {
			ecran.popup("Erreur Technique", "Notre application est Hors Service", JOptionPane.ERROR_MESSAGE);
		}
	}
}
