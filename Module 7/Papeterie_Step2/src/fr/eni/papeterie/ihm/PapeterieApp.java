package fr.eni.papeterie.ihm;

import javax.swing.SwingUtilities;

import fr.eni.papeterie.ihm.controller.ControllerArticle;
import fr.eni.papeterie.ihm.controller.ControllerCatalogue;

public class PapeterieApp {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				ControllerArticle cArticle = new ControllerArticle();
				cArticle.showEcranArticle();

				ControllerCatalogue cCatalogue = new ControllerCatalogue();
				cCatalogue.showEcranCatalogue();
			}
		});
	}
}
