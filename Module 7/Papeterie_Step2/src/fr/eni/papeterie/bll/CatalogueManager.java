package fr.eni.papeterie.bll;

import java.util.List;

import fr.eni.papeterie.bo.Article;
import fr.eni.papeterie.bo.Ramette;
import fr.eni.papeterie.bo.Stylo;
import fr.eni.papeterie.dal.ArticleDAO;
import fr.eni.papeterie.dal.DALException;
import fr.eni.papeterie.dal.DAOFactory;

/**
 * Design Pattern Singleton -- unique instance
 * 
 * @author ENI Ecole
 *
 */
public class CatalogueManager {
	private static CatalogueManager instance;
	private ArticleDAO daoArticle;

	public static CatalogueManager getInstance() throws BLLException{
		if (instance == null) {
			instance = new CatalogueManager();
		}
		return instance;
	}

	private CatalogueManager() {
		// Récupération de l'instance de ArticleDAO
		daoArticle = DAOFactory.getArticleDAO();
	}

	public List<Article> getCatalogue() throws BLLException {
		try {
			return daoArticle.selectAll();
		} catch (DALException e) {
			throw new BLLException("Aucun article -- " + e.getMessage());
		}
	}

	public void addArticle(Article a) throws BLLException {
		try {
			validerArticle(a);
			daoArticle.insert(a);
		} catch (DALException e) {
			throw new BLLException(e.getMessage());
		}
	}

	public void updateArticle(Article a) throws BLLException {
		try {
			validerArticle(a);
			daoArticle.update(a);
		} catch (DALException e) {
			throw new BLLException(e.getMessage());
		}
	}

	public void removeArticle(int index) throws BLLException {
		if (index > 0) {
			try {
				daoArticle.delete(index);
			} catch (DALException e) {
				throw new BLLException(e.getMessage());
			}
		} else {
			throw new BLLException("Votre index " + index + " n'existe pas");
		}
	}

	/**
	 * Vérifier l'ensemble des contraintes métiers sur l'objet Article avant
	 * sauvegarde
	 * 
	 * @param a
	 * @throws BLLException
	 *             -- retourne cette exception dans le cas d'un échec
	 */
	public void validerArticle(Article a) throws BLLException {
		if (a == null) {
			throw new BLLException("L'objet doit être créé");
		}
		boolean valide = true;

		StringBuilder msgErrors = new StringBuilder("\n");
		if (a.getReference() == null || a.getReference().trim().length() == 0) {
			msgErrors.append("La référence est obligatoire\n");
			valide = false;
		}

		if (a.getMarque() == null || a.getMarque().trim().length() == 0) {
			msgErrors.append("La marque est obligatoire\n");
			valide = false;
		}

		if (a.getDesignation() == null || a.getDesignation().trim().length() == 0) {
			msgErrors.append("La désignation est obligatoire\n");
			valide = false;
		}

		if (a.getPrixUnitaire() < 0) {
			msgErrors.append("Le prix unitaire est positif ou nul\n");
			valide = false;
		}

		if (a.getQteStock() <= 0) {
			msgErrors.append("La quantité est positive\n");
			valide = false;
		}

		if (a instanceof Stylo) {
			Stylo s = (Stylo) a;
			if (s.getCouleur() == null || s.getCouleur().trim().length() == 0) {
				msgErrors.append("La couleur du stylo est obligatoire\n");
				valide = false;
			}
		}

		if (a instanceof Ramette) {
			Ramette r = (Ramette) a;
			if (r.getGrammage() <= 0) {
				msgErrors.append("Le grammage est positif\n");
				valide = false;
			}
		}

		if (!valide) {
			throw new BLLException(msgErrors.toString());
		}

	}

	public Article getArticle(int index) throws BLLException {
		if (index > 0) {
			try {
				return daoArticle.selectById(index);
			} catch (DALException e) {
				throw new BLLException(e.getMessage());
			}
		} else {
			throw new BLLException("Votre index " + index + " n'existe pas");
		}
	}
}
