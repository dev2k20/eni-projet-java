package fr.eni.papeterie.bo;

import java.util.ArrayList;
import java.util.List;

public class Panier {
	// Attribut association
	private List<Ligne> lignesPanier;
	private float montant;

	public Panier() {
		lignesPanier = new ArrayList<Ligne>();
		montant = 0;
	}

	public List<Ligne> getLignesPanier() {
		return lignesPanier;
	}

	public float getMontant() {
		return montant;
	}

	public void addLigne(Article art, int qte) {
		// Creer la ligne
		Ligne newLigne = new Ligne(art, qte);
		lignesPanier.add(newLigne);
		// Mise à jour du montant
		montant += newLigne.getPrix() * qte;
	}

	public Ligne getLigne(int index) {
		if (index >= 0 && index < lignesPanier.size()) {
			return lignesPanier.get(index);
		}
		return null;
	}

	public void updateLigne(int index, int newQte) {
		//Permet de récupérer la référence en mémoire
		Ligne current = getLigne(index);
		if (current != null) {
			// Mettre à jour le montant
			montant -= current.getPrix() * current.getQte();
			current.setQte(newQte);
			montant += current.getPrix() * newQte;
		}
	}

	public void removeLigne(int index) {
		if (index >= 0 && index < lignesPanier.size()) {
			Ligne supprime = lignesPanier.remove(index);
			// Mettre à jour le montant
			montant -= supprime.getPrix() * supprime.getQte();
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((lignesPanier == null) ? 0 : lignesPanier.hashCode());
		result = prime * result + Float.floatToIntBits(montant);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Panier other = (Panier) obj;
		if (lignesPanier == null) {
			if (other.lignesPanier != null)
				return false;
		} else if (!lignesPanier.equals(other.lignesPanier))
			return false;
		if (Float.floatToIntBits(montant) != Float.floatToIntBits(other.montant))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Panier :\n\n");
		for (Ligne ligne : lignesPanier) {
			builder.append("ligne ");
			builder.append(lignesPanier.indexOf(ligne));
			builder.append(" :\t");
			builder.append(ligne);
			builder.append("\n");
		}
		builder.append("\nValeur du panier : " + montant);
		builder.append("\n\n");
		return builder.toString();
	}

}
