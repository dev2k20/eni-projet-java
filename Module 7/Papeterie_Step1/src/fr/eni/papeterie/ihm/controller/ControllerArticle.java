package fr.eni.papeterie.ihm.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JOptionPane;

import fr.eni.papeterie.bll.BLLException;
import fr.eni.papeterie.bll.CatalogueManager;
import fr.eni.papeterie.bo.Article;
import fr.eni.papeterie.bo.Ramette;
import fr.eni.papeterie.bo.Stylo;
import fr.eni.papeterie.ihm.view.EcranArticle;

public class ControllerArticle {

	// Partie dynamique de l'IHM
	/**
	 * Client lourd -- chargement par défaut des données en base
	 */
	private List<Article> catalogue;
	private int indexCatalogue;
	// Article courant
	private Integer idCourant;

	private CatalogueManager mger;
	
	private EcranArticle ecran;
	
	public ControllerArticle() {
		ecran = new EcranArticle();
	}

	/**
	 * 
	 * Mise à jour de l'affichage en fonction de l'article -- si valeur null ->
	 * nouvel article
	 */
	private void afficherArticle(Article a) {
		String ref, marque, design, pu, qs, couleur;
		Integer grammage;
		if (a == null) {
			// nouvel article
			idCourant = null;
			ref = "";
			marque = "";
			design = "";
			pu = "0";
			qs = "0";
			couleur = null;
			grammage = 80;
			ecran.getRadioRamette().setEnabled(true);
			ecran.getRadioStylo().setEnabled(true);
		} else {
			idCourant = a.getIdArticle();
			ref = a.getReference();
			marque = a.getMarque();
			design = a.getDesignation();
			pu = String.valueOf(a.getPrixUnitaire());
			qs = String.valueOf(a.getQteStock());
			if (a instanceof Stylo) {
				Stylo s = (Stylo) a;
				couleur = s.getCouleur();
				grammage = null;
			} else {
				Ramette r = (Ramette) a;
				grammage = r.getGrammage();
				couleur = null;
			}
			ecran.getRadioRamette().setEnabled(false);
			ecran.getRadioStylo().setEnabled(false);
		}

		// Mise à jour des champs
		ecran.getTxtReference().setText(ref);
		ecran.getTxtMarque().setText(marque);
		ecran.getTxtDesignation().setText(design);
		ecran.getTxtPrix().setText(pu);
		ecran.getTxtStock().setText(qs);
		if (couleur != null) {
			// cas stylo
			ecran.selectedStyloOrRamette(true, false);
			ecran.selectedCouleurGrammage(couleur, 80);
		} else {
			// cas ramette
			ecran.selectedStyloOrRamette(false, true);
			ecran.selectedCouleurGrammage(couleur, grammage);
		}
	}

	/**
	 * Article manipulé
	 * 
	 * @return
	 */
	public Article getArticle() {
		Article art = null;
		if (idCourant == null) {
			// nouvel article
			if (ecran.getRadioRamette().isSelected()) {
				art = new Ramette();
			} else {
				art = new Stylo();
			}
		} else {
			// article affiché actuellement
			art = catalogue.get(indexCatalogue);
		}
		art.setReference(ecran.getTxtReference().getText());
		art.setMarque(ecran.getTxtMarque().getText());
		art.setDesignation(ecran.getTxtDesignation().getText());
		try {
			// validation de surface
			art.setPrixUnitaire(Float.parseFloat(ecran.getTxtPrix().getText()));
			art.setQteStock(Integer.parseInt(ecran.getTxtStock().getText()));

			if (ecran.getCboCouleur().isEnabled()) {
				((Stylo) art).setCouleur((String) ecran.getCboCouleur().getSelectedItem());
			} else {
				// getChk80().isSelected()? 80 : 100
				// Test d'affectation --
				// Si champs 80 est sélectionné la valeur 80 est utilisée
				// Sinon 100
				((Ramette) art).setGrammage(ecran.getChk80().isSelected() ? 80 : 100);
			}

		} catch (Exception e) {
			ecran.popup("Erreur Formulaire", "Veuillez vérifier votre prix unitaire et votre quantité",
					JOptionPane.ERROR_MESSAGE);
			art = null;
		}

		return art;
	}

	public void addActionButton() {
		ecran.getRadioStylo().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ecran.selectedStyloOrRamette(true, false);
			}
		});

		ecran.getRadioRamette().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ecran.selectedStyloOrRamette(false, true);
			}
		});

		ecran.getBtnPrecedent().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				precedent();
			}
		});

		ecran.getBtnNouveau().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				nouveau();
			}
		});

		ecran.getBtnEnregistrer().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				enregistrer();
			}
		});

		ecran.getBtnSupprimer().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				supprimer();
			}
		});

		ecran.getBtnSuivant().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				suivant();
			}
		});
	}

	private void precedent() {
		int size = catalogue.size();
		if (size > 0) {
			indexCatalogue--;
			if (indexCatalogue < 0) {
				indexCatalogue = size - 1;
			}
			afficherArticle(catalogue.get(indexCatalogue));
		}
	}

	private void nouveau() {
		indexCatalogue = catalogue.size();
		afficherArticle(null);
	}

	/*
	 * insert ou update
	 */
	private void enregistrer() {
		Article articleAffiche = getArticle();
		if (articleAffiche != null) {
			try {
				if (articleAffiche.getIdArticle() == null) {
					// nouvel article
					mger.addArticle(articleAffiche);
					catalogue.add(articleAffiche);
					afficherArticle(articleAffiche);
					ecran.popup("Info", "L'article a été ajouté", JOptionPane.INFORMATION_MESSAGE);
				} else {
					// Mise à jour
					mger.updateArticle(articleAffiche);
					// mise à jour la liste
					catalogue.set(indexCatalogue, articleAffiche);
					ecran.popup("Info", "L'article a été mis à jour", JOptionPane.INFORMATION_MESSAGE);
				}
			} catch (BLLException e) {
				ecran.popup("Erreur Enregistrement", e.getMessage(), JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	private void supprimer() {
		if (idCourant != null) {
			try {
				mger.removeArticle(idCourant);
				catalogue.remove(indexCatalogue);
				ecran.popup("Info", "L'article a été supprimé", JOptionPane.INFORMATION_MESSAGE);
				if (indexCatalogue < catalogue.size()) {
					// il y a un suivant
					afficherArticle(catalogue.get(indexCatalogue));
				} else if (indexCatalogue > 0) {
					indexCatalogue--;
					afficherArticle(catalogue.get(indexCatalogue));
				} else {
					// plus rien à afficher
					nouveau();
				}

			} catch (BLLException e) {
				ecran.popup("Erreur Suppression", e.getMessage(), JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	private void suivant() {
		int size = catalogue.size();
		if (size > 0) {
			indexCatalogue++;
			if (indexCatalogue >= size) {
				indexCatalogue = 0;
			}
			afficherArticle(catalogue.get(indexCatalogue));
		}
	}

	private void initData() throws BLLException {
		mger = CatalogueManager.getInstance();
		catalogue = mger.getCatalogue();
	}

	/**
	 * Affichage par défaut de l'application
	 */
	public void showEcranArticle() {
		try {
			initData();
			ecran.initIhm();
			addActionButton();
			if (catalogue.size() > 0) {
				indexCatalogue = 0;
				afficherArticle(catalogue.get(indexCatalogue));
			} else {
				ecran.popup("Info", "Il n'y a pas d'article en base", JOptionPane.INFORMATION_MESSAGE);
				indexCatalogue = -1;
				nouveau();
			}
		} catch (BLLException e) {
			ecran.popup("Erreur Technique", "Notre application est Hors Service", JOptionPane.ERROR_MESSAGE);
			// Quitter l'application
			System.exit(0);
		}
	}
}
