package fr.eni.papeterie.dal;

import fr.eni.papeterie.dal.jdbc.ArticleDAOJdbcImpl;

/**
 * Design Pattern Factory
 * -- Fabrique d'instance 
 * -- Centralise les instances de la couche DAL
 * @author ENI Ecole
 *
 */
public class DAOFactory {
	//Instance d'ArticleDAO
	public static ArticleDAO getArticleDAO() {
		return new ArticleDAOJdbcImpl();
	}

}
